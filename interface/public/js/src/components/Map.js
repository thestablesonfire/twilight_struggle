var map = angular.module('map', []);

map.directive('tsMap', function() {
    return {
        restrict: 'E',
        templateUrl: 'map.html',
        controller: function($window, $timeout) {
            var thisMap = this;
            thisMap.countries = [1,2,3,4,5,6,7,8,9];
            thisMap.zoomPctg = 100;
            thisMap.zoomPoint = {
                x: 0,
                y: 0
            };
            thisMap.mapStyle = {
                'background-size': thisMap.zoomPctg + '% auto'
            };

            var bgWidth = 3400;
            var bgHeight = 2200;
            var bgRatio = bgHeight/bgWidth;

            var viewportWidth = angular.element($window).innerWidth();
            var viewportHeight = angular.element($window).innerHeight();

            var bgPosX = 0;
            var bgPosY = 0;

            console.log(viewportWidth, viewportHeight, bgRatio);

            var MAX_ZOOM = 500;
            var MIN_ZOOM = 50;
            var zoomIncrement = 25;

            angular.element($(document)).on("mousewheel", function(e) {
                var oldZoom = thisMap.zoomPctg;

                if(e.originalEvent.wheelDelta / 120 > 0) {
                    if( thisMap.zoomPctg + zoomIncrement <= MAX_ZOOM )
                    thisMap.zoomPctg += zoomIncrement;
                } else {
                    if( thisMap.zoomPctg - zoomIncrement >= MIN_ZOOM )
                    thisMap.zoomPctg -= zoomIncrement;
                }

                adjustZoom(oldZoom);

                $timeout(function() {
                    thisMap.mapStyle['background-size'] = thisMap.zoomPctg + '% auto';
                    thisMap.mapStyle['background-position'] = bgPosX + 'px ' + bgPosY + 'px';
                });
            });

            angular.element($(document)).on("mousemove", function(e){
                thisMap.zoomPoint.x = e.screenX;
                thisMap.zoomPoint.y = e.screenY;
            });

            function adjustZoom(oldZoom) {
                /*
                scalechange = newscale - oldscale;
                offsetX = -(zoomPointX * scalechange);
                offsetY = -(zoomPointY * scalechange);
                */

                var scaleChange = (thisMap.zoomPctg - oldZoom)/100;
                console.log(scaleChange);
                var zoomPointX = thisMap.zoomPoint.x;
                var zoomPointY = thisMap.zoomPoint.y;
                console.log(zoomPointX, zoomPointY);

                var offsetX = -(zoomPointX * scaleChange);
                var offsetY = -(zoomPointY * scaleChange);
                console.log(offsetX, offsetY);

                bgPosX += offsetX;
                bgPosY += offsetY;
            }
        }, // END CONTROLLER
        controllerAs: 'mapCtrl'
    };
});

map.controller('countryController', ['$scope', function($scope) {
    var thisCountry = this;

    $scope.init = function(name) {
        thisCountry.name = name;
    };
}]);

