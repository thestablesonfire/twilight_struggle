// README:
// This project is set up to compile individual scss files to css files.
// You must add your individual css files to the header for it to be included
// Please try to include your files only on pages they must be included on
// I have removed js concatenation

var gulp = require('gulp');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var sass = require('gulp-sass');

var FTPdelay = 500;

gulp.task('js', function() {
  setTimeout(function(){
    return gulp.src(['public/js/src/*.js', 'public/js/src/components/*.js'])
      .pipe(concat('interface.js'))
      .pipe(gulp.dest('public/js'));
  }, FTPdelay);
});

gulp.task('scss', function() {
  setTimeout(function(){
return gulp.src('public/css/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('public/css'));
  }, FTPdelay);
});

// This is the task that will run every time you run "gulp" after everything is installed.
gulp.task('default', function () {
  gulp.start(['js','scss']);
  gulp.watch('public/js/src/**/*.js', ['js']);
  gulp.watch('public/css/**/*.scss', ['scss']);
});
