// README:
// This project is set up to compile individual scss files to css files.
// You must add your individual css files to the header for it to be included
// Please try to include your files only on pages they must be included on
// I have removed js concatenation

var gulp = require('gulp');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');

var tap = require('gulp-tap');
var esformatter = require('esformatter');
var fs = require('fs');

gulp.task('format', function () {
    var options = {
        "indent" : {
            "value" : '  ',
            "ArgumentListFunctionExpression": 1
        },
        "lineBreak" : {
            "before" : {
                "BlockStatement" : 1,
                "FunctionDeclaration" : ">=1",
                "FunctionExpression": 0,
                "FunctionDeclarationOpeningBrace": 0,
                "FunctionDeclarationClosingBrace": 1,
                "FunctionExpressionOpeningBrace": 0,
                "FunctionExpressionClosingBrace": 1,
                "IfStatementClosingBrace": 1,
                "ThisExpression": ">=1",
              "SwitchStatement": 2
            },
            "after" : {
                "FunctionDeclaration": 1,
                "FunctionDeclarationOpeningBrace": 1,
                "FunctionDeclarationClosingBrace": 1,
                "FunctionExpressionOpeningBrace": 1,
                "FunctionExpressionClosingBrace": 0,
                "IfStatementOpeningBrace": 1
            },
            "value": "\n"
        },
        "whiteSpace" : {
            "before": {
                "ArgumentListFunctionExpression": 1,
                "ArgumentListArrayExpression": 1,
                "ArgumentListObjectExpression": 1,
                "ArrayExpressionOpening": 1,
                "ArrayExpressionClosing": 1,
                "ArrayExpressionComma": 0,
                "ArgumentList": 0,
                "ParameterList": 0,
                "ElseStatement": 1,
                "ForStatementExpressionClosing": 1,
                "ForStatementExpressionOpening": 1,
                "FunctionExpression": 1,
                "IfStatementConditionalClosing": 1,
                "IfStatementConditionalOpening": 1,
                "IfStatementClosingBrace": 0,
                "IfStatementOpeningBrace": 1
            },
            "after": {
                "ArrayExpressionOpening": 1,
                "ArrayExpressionComma": 1,
                "ArgumentComma": 1,
                "ArgumentList": 0,
                "ArgumentListFunctionExpression": 1,
                "ArgumentListArrayExpression": 1,
                "ArgumentListObjectExpression": 1,
                "ForStatementExpressionClosing": 0,
                "ForStatementExpressionOpening": 1,
                "FunctionExpressionClosingBrace": 0,
                "FunctionName": 1,
                "IfStatementConditionalOpening": 1,
                "IfStatementOpeningBrace": 0
            },
            "removeTrailing": false
        }
    };

    gulp.src(['src/classes/data/*.js', 'src/classes/*.js', 'src/TwilightStruggle.js'])
        .pipe(tap(function(file, t) {
            console.log(file.path);
            var codeStr = fs.readFileSync(file.path).toString();
            var formattedCode = esformatter.format(codeStr, options);
            fs.writeFile(file.path, formattedCode);
        }));
});

var FTPdelay = 500;

gulp.task('js', function() {
  setTimeout(function(){
    return gulp.src(['src/classes/data/*.js', 'src/classes/*.js', 'src/TwilightStruggle.js'])
      .pipe(concat('api.js'))
      .pipe(gulp.dest('.'));
  }, FTPdelay);
});

// This is the task that will run every time you run "gulp" after everything is installed.
gulp.task('default', function () {
  //gulp.start('js');
  //gulp.watch('src/**/*.js', ['js']);
});
