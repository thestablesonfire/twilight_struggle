define([
  'modules/Regions',
  'modules/Utilities',
  'modules/Players',
  'modules/CardTypes',
  'modules/Countries',
  'modules/DebugFlags'
], function(Regions, Utilities, Players, CardTypes, Countries, DebugFlags) {
  return function(config) {
    var cardEffectsDebug = DebugFlags.cardEffectsDebug;

    /*
     Scoring Guidelines
     Presence: Controls 1 country in region
     Domination: More countries and battlegrounds in region
     Must have one non-battleground and one battlegorund
     Control: More countries and all battlegrounds
     +1 for each country adjacent to enemy superpower
     +1 per battleground
     */

    var actionRoundTrack = config.actionRoundTrack;
    var chinaCard = config.chinaCard;
    var deck = config.deck;
    var defconTrack = config.defconTrack;
    var discard = config.discard;
    var gameplayController = config.gameplayController;
    var inputHandler = config.inputHandler;
    var map = config.gameMap;
    var milOps = config.milOps;
    var players = config.players;
    var turnTrack = config.turnTrack;
    var vpTrack = config.vpTrack;

    this.AsiaScoring = function() {
      if ( DebugFlags.cardDebug ) {
        console.log('Card Effects: Asia Scoring');
      }
      var asia = map.getRegion(Regions.Asia);
      regionScoring(asia);
      updateStep();
    };

    this.EuropeScoring = function() {
      if ( DebugFlags.cardDebug ) {
        console.log('Card Effects: Europe Scoring');
      }
      var europe = map.getRegion(Regions.Europe);
      regionScoring(europe);
      updateStep();
    };

    this.MiddleEastScoring = function() {
      if ( DebugFlags.cardDebug ) {
        console.log('Card Effects: Middle East Scoring');
      }
      var middleEast = map.getRegion(Regions.MiddleEast);
      regionScoring(middleEast);
      updateStep();
    };

    // Degrade DEFCON 1 level,
    // US gets VPs equal to 5 - DEFCON
    this.DuckandCover = function() {
      if ( DebugFlags.cardDebug ) {
        console.log('Card Effects: Duck and Cover');
      }
      var defconLevel = defconTrack.decreaseStatus();
      vpTrack.increaseVP(defconLevel);
      updateStep();
    };


    // The USSR must randomly discard a card
    // IF the card has a US event, event occurs immediately
    // IF USSR or Neutral card must be discarded
    this.FiveYearPlan = function() {
      if ( DebugFlags.cardDebug ) {
        console.log('Card Effects: Five Year Plan');
      }
      var USSRCard = players.USSR.randomlyDiscardCard();
      if ( USSRCard.getEventType() === CardTypes.USA ) {
        if ( DebugFlags.cardDebug ) {
          console.log('USA Card');
        }
        var USSRCardID = USSRCard.getID();

        this[USSRCardID]();
        discard.addCard(USSRCard);
      } else {
        if ( DebugFlags.cardDebug ) {
          console.log('Not USA Card');
        }
        updateStep();
      }
    };

    // TODO: Unplayable if 'The Iron Lady' is in effect
    // Remove US influence in Western Europe by a total of 3 influence points
    // removing no more than 2 per country
    this.SocialistGovernments = function() {
      if ( DebugFlags.cardDebug ) {
        console.log('Card Effects: Socialist Governments');
      }
      var countries = [];

      function SocialistGovernmentsCallback (obj) {

        var FIRST_SELECTED = 0;
        var SECOND_SECLECTED = 1;
        var MAX_INFLUENCE_REMOVABLE = 3;

        if ( DebugFlags.cardDebug ) {
          console.log('Card Effects: Socialist Governments Callback');
        }

        console.log('OBJ:   ', obj);

        var country = obj.country;

        // Check if country is in Western Europe
        var countryObj = map.getCountry(country);
        var WEurope = map.getRegion(Regions.WesternEurope);
        if ( WEurope.countryIsInRegion(countryObj) ) {
          var first = countries.indexOf(countryObj);
          var last = countries.lastIndexOf(countryObj);
          // Make sure country hasn't already been added to list twice
          if ( first !== last ) {
            console.error('ERR, 2 influence already removed from country ' + country);
          } else {
            // Make sure country has enough influence to remove
            var influence = countryObj.getInfluence();
            influence = influence[Players.USA];
            if ( ~first ) {
              influence > FIRST_SELECTED ? influence = true : influence = false;
            } else {
              influence > SECOND_SECLECTED ? influence = true : influence = false;
            }
            if ( influence ) {
              countries.push(countryObj);
            } else {
              console.error('ERR: not enough US influence present in country to remove');
            }
          }
        } else {
          console.error('ERR: country is NOT in Western Europe');
          return false;
        }

        // Make sure its possible to remove 3 USA influence from WEurope
        // If not, just use the max possible number
        var maxPossible = WEurope.getMaxActionsToRemoveInfluence(Players.USA, 2);
        maxPossible = Math.min(MAX_INFLUENCE_REMOVABLE, maxPossible);

        // If max actions made, remove the influence and disable the callback
        if ( countries.length == maxPossible ) {
          if ( DebugFlags.cardDebug ) {
            console.log('all countries selected, removing influence');
          }
          for (var country in countries) {
            countries[country].removeInfluence(Players.USA);
          }
          inputHandler.setActiveFunction(null);
          if ( DebugFlags.cardDebug ) {
            console.log('INACTIVE: Socialist Governments Callback');
          }
          updateStep();
        }
      }
      inputHandler.setActiveFunction(SocialistGovernmentsCallback);
      if ( DebugFlags.cardDebug ) {
        console.log('ACTIVE: Socialist Governments Callback');
      }
    };

    // Unless the US immediately discards a card with an Operations value of 3 or more,
    // remove all US Influence from West Germany.
    this.Blockade = function() {
      if ( DebugFlags.cardDebug ) {
        console.log('Card Effects: Blockade');
      }

      function blockadeCallback (obj) {
        if ( obj ) {
          var MIN_OPS_VALUE = 3;

          if ( DebugFlags.cardDebug ) {
            console.log('USA is discarding', obj.card);
          }
          // If user did select a card,
          var cardName = obj.card;
          var card = players[Players.USA].getCardFromHand(cardName);
          var opsVal = card.getOpsValue();

          // and card has a great enough ops value
          if ( opsVal >= MIN_OPS_VALUE ) {
            // Discard the card
            players[Players.USA].discardCard(cardName);
            inputHandler.setActiveFunction(null);
          } else {
            console.error('ERR: Card Ops value was only ' + opsVal + ". Must be at least 3.");
          }
        } else {
          // Otherwise remove all influece from wGermany
          if ( DebugFlags.cardDebug ) {
            console.log('USA refused to discard. Removing all influence from West Germany');
          }
          var wGermany = map.getCountry(Countries.WestGermany);
          wGermany.removeAllInfluence(Players.USA);
        }
        updateStep();
      }
      inputHandler.setActiveFunction(blockadeCallback);
    };

    // North Korea invades South Korea.
    // - USSR Card
    // - Roll a die and subtract (-1) from the die roll
    //  for every US controlled country adjacent to South Korea.
    //
    // On a modified die roll of 4-6:
    //      - The USSR receives 2 VP and replaces all US Influence in
    //        South Korea with USSR Influence.
    //      - The USSR adds 2 to its Military Operations Track.
    this.KoreanWar = function() {
      if ( DebugFlags.cardDebug ) {
        console.log('Card Effects: Korean War');
      }

      var NUM_DIE_ROLLED = 1;
      var MIN_DIE_ROLL = 4;
      var USSR_MILOPS_GAINED = 2;
      var USSR_VPS_GAINED = 2;

      // Roll die
      var dieRoll = Utilities.rollDie(NUM_DIE_ROLLED)[0];
      var sKorea = map.getCountry(Countries.SouthKorea);
      var sKoreaConnections = sKorea.getAllConnections();

      // Subtract 1 for every US controlled country adjacent to South Korea
      for (var country in sKoreaConnections) {
        if ( sKoreaConnections[country].getIsControlledBy(Players.USA) ){
          dieRoll--;
        }
      }
      if ( DebugFlags.cardDebug ) {
        console.log('\tDie roll final', dieRoll);
      }

      // On a modified die roll of 4-6
      if ( dieRoll >= MIN_DIE_ROLL ) {
        // Give 2 VPs to USSR
        vpTrack.decreaseVP(USSR_VPS_GAINED);

        // Replace all USA influence with USSR influence
        var usInfluenceNumber = sKorea.getInfluence()[Players.USA];
        sKorea.removeAllInfluence(Players.USA);
        for ( var i = 0; i < usInfluenceNumber; i++ ) {
          sKorea.addInfluence(Players.USSR);
        }

        // Add 2 to milops for USSR
        milOps.addCurrentMilOps(USSR_MILOPS_GAINED, Players.USSR);
      } else {
        // Otherwise nothing happens
        if ( DebugFlags.cardDebug ) {
          console.log('Nothing happens');
        }
      }
      updateStep();
    };

    // Private Methods
    function updateStep () {
      gameplayController.updateStep();
    }

    function regionScoring (region) {
      var regionScoreUSA = region.getRegionScore(Players.USA);
      var regionScoreUSSR = region.getRegionScore(Players.USSR);
      if ( DebugFlags.cardDebug ) {
        console.log('USA', regionScoreUSA, 'USSR', regionScoreUSSR);
      }
      var difference = regionScoreUSA - regionScoreUSSR;
      vp.increaseVP(difference);
    }
  };
});
