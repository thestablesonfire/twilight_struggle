define([
  'modules/GameplaySteps',
  'CardEffects',
  'modules/DebugFlags',
  'modules/CardTitles',
  'modules/Countries',
  'modules/Players',
  'modules/OperationTypes',
  'modules/Utilities'
], function(GameplaySteps, CardEffects, DebugFlags, Cards, Countries, Players, OperationTypes, Utilities) {
  return function(config) {
    var thisController = this,
      gameplayDebug = DebugFlags.gameplayDebug,

      // TODO: We need to make this a loop and not a recursive call

      curStep = -1,
      steps = [
        GameplaySteps.ImproveDefconStatus,
        GameplaySteps.DealCards,
        GameplaySteps.HeadlinePhaseA,
        GameplaySteps.HeadlinePhaseB,
        GameplaySteps.ActionRounds,
        GameplaySteps.ActionRoundCheck,
        GameplaySteps.CheckMilOpsStatus,
        GameplaySteps.RevealHeldCard,
        GameplaySteps.FlipChinaCard,
        GameplaySteps.AdvanceTurnMarker
      ],

      // CONST
      NUM_STEPS = steps.length,

      // Game Objects
      actionRoundTrack = config.actionRoundTrack,
      chinaCard = config.chinaCard,
      deck = config.deck,
      defconTrack = config.defconTrack,
      discard = config.discard,
      inputHandler = config.inputHandler,
      gameMap = config.gameMap,
      milOps = config.milOps,
      players = config.players,
      turnTrack = config.turnTrack,
      vpTrack = config.victoryPointTrack,

      headlineCards = {
        USA: null,
        USSR: null
      },

      headlineOrder = {
        first: null,
        second: null
      },

      cardEffects = new CardEffects({
        gameplayController: this,
        inputHandler: inputHandler,
        players: players,
        defconTrack: defconTrack,
        deck: deck,
        discard: discard,
        actionRoundTrack: actionRoundTrack,
        milOps: milOps,
        chinaCard: chinaCard,
        turnTrack: turnTrack,
        vpTrack: vpTrack,
        gameMap: gameMap
      });

    // Advances the current step and performs any maintenance needed
    thisController.updateStep = function() {
      curStep = (curStep + 1) % NUM_STEPS;
      if ( gameplayDebug ) {
        console.log('Current Step: ' + steps[curStep]);
      }

      switch (steps[curStep]) {
        case GameplaySteps.ImproveDefconStatus:
          improveDEFCONStatus();
          break;
        case GameplaySteps.DealCards:
          dealCards();
          break;
        case GameplaySteps.HeadlinePhaseA:
          inputHandler.setActiveFunction(headlineCardSelected);
          break;
        case GameplaySteps.HeadlinePhaseB:
          playSecondHeadlineCard();
          break;
        case GameplaySteps.ActionRounds:
          var actionRound = actionRoundTrack.getValue();
          console.log('\nAction Round: ', actionRound);

          // If only the first action round, we need to discard
          // the second headline card from the headline phase
          if ( actionRound === 1 ) {
            actionRoundTrack.setPhasingPlayer(Players.USSR);
          }
          inputHandler.setActiveFunction(playActionPhaseCard);

          break;
        case GameplaySteps.ActionRoundCheck:
          var curRound = actionRoundTrack.increaseActionRound();
          if ( curRound <= actionRoundTrack.getMaxActionRound() ) {
            console.log('\tNot last action round, new action round');
            //previousStep();
          }
          break;
      }
    };

    // ACTION PHASE METHODS //

    // This is the function that is called by the front-end to play an action
    // phase card. The object can be a card, or it can be an operation and a
    // card. The operation indicates which operation the card should be
    // used for.

    function playActionPhaseCard (obj) {
      // If the object indicates an operation
      if ( obj.operation ) {
        playCardAsOperation(obj);
      }
      // Otherwise play the card
      else {
        var phasingPlayer = players[actionRoundTrack.getPhasingPlayer()];
        var card = phasingPlayer.getCardFromHand(obj.card);
        if ( gameplayDebug ) {
          console.log('\nPlaying Action Phase Card: ' + card.getTitle());
        }
        var cardID = card.getID();
        cardEffects[cardID]();
      }
    }

    // HEADLINE PHASE METHODS //

    // When a player indicates which card they would like to play as a headline
    // card, store it in the object that keeps track of them

    function headlineCardSelected (obj) {
      var card = obj.card;
      var player = obj.player;
      var playerName = Players[player];

      console.log(player);

      if(headlineCards[player]) {
        console.error('error! Player ' + playerName + ' has already selected a headline card.');
      } else {
        headlineCards[player] = players[playerName].getCardFromHand(card);
        if(headlineCards[player]) {
            if ( gameplayDebug ) {
                console.log('\t' + player + ": " + headlineCards[player].getTitle());
            }

            // Remove card from player's hand
            var selectedIndex = players[playerName].getCardIndexFromHandByTitle(headlineCards[player].getTitle());
            players[playerName].removeCardFromHand(selectedIndex);
            // If both headline cards have been selected, play them in order
            if ( headlineCards[Players.USA] && headlineCards[Players.USSR] ) {
                determineHeadlineCardOrder();
            }
        } else {
            // Otherwise print error message
            console.error('error! This card is not in the ' + playerName + ' player\'s hand. Please make a valid' +
              ' selection');
        }
      }
    }

    // This plays the headline card with the higher ops value

    function playFirstHeadlineCard () {
      if ( gameplayDebug ) {
        console.log('\nPlaying First Headline Card');
        console.log('\tPlayer:', actionRoundTrack.getPhasingPlayer());
        console.log('\tCard:', headlineOrder.first.getTitle());
      }

      // Play the first card
      var firstCardID = headlineOrder.first.getID();
      cardEffects[firstCardID]();
      // Remove first from headline order struct
      headlineOrder.first = null;
    }

    // This plays the headline card with the lower ops value

    function playSecondHeadlineCard () {
      actionRoundTrack.togglePhasingPlayer();
      if ( gameplayDebug ) {
        console.log('\nPlaying Second Headline Card');
        console.log('\tPlayer:', actionRoundTrack.getPhasingPlayer());
        console.log('\tCard:', headlineOrder.second.getTitle());
      }
      var secondCardID = headlineOrder.second.getID();
      cardEffects[secondCardID]();
    }

    // This determines in which order the headline cards should be played

    function determineHeadlineCardOrder () {
      var USSROps,
        USAOps,
        USACard = headlineCards[Players.USA],
        USSRCard = headlineCards[Players.USSR];

      // If the player played a scoring card, their ops value is 0
      if ( USSRCard.isScoringCard ) {
        USSROps = 0;
      } else {
        USSROps = USSRCard.getOpsValue();
      }
      if ( USACard.isScoringCard ) {
        USAOps = 0;
      } else {
        USAOps = USACard.getOpsValue();
      }
      if ( gameplayDebug ) {
        console.log('Determining Headline Card Play Order');
        console.log('\tOps Val USA: ' + USAOps);
        console.log('\tOps Val USSR: ' + USSROps);
      }

      var first = null,
        discardUSA = false;
      if ( USAOps >= USSROps ) {
        if ( gameplayDebug ) {
          console.log('\tUSA Plays First');
        }
        headlineOrder.first = USACard;
        headlineOrder.second = USSRCard;
        actionRoundTrack.setPhasingPlayer(Players.USA);
      } else {
        if ( gameplayDebug ) {
          console.log('\tUSSR Plays First');
        }
        headlineOrder.first = USSRCard;
        headlineOrder.second = USACard;
        actionRoundTrack.setPhasingPlayer(Players.USSR);
      }
      playFirstHeadlineCard();
    }

    // This function is called whenever, during an action phase,
    // the user indicated that they would like to play a card as
    // and operation

    function playCardAsOperation (obj) {
      var operationType = obj.operation,
        card = obj.card;
      /* Operation types are:
       - Place Influence
       = Realignment Rolls
       - Coup Attempts
       - Space Race
       */
      console.log('Playing Card: \'' + card + '\' as Operation: ' + operationType);

      switch (operationType) {
        case OperationTypes.PlaceInfluence:
          operationPlaceInfluence(obj);

          // DEBUG
          inputHandler.activeFunction({
            country: Countries.WestGermany
          });
          inputHandler.activeFunction({
            country: Countries.WestGermany
          });

          // Should pass
          inputHandler.activeFunction({
            country: Countries.Algeria
          });
          // Ends turn early
          //inputHandler.activeFunction({end: true});
          // Should Fail
          //inputHandler.activeFunction({country: Countries.Yugoslavia});
          //inputHandler.activeFunction({country: Countries.EastGermany});
          break;
        case OperationTypes.RealignmentRoll:
          operationRealignmentRoll(obj);
          break;
        case OperationTypes.CoupAttempt:
          break;
        case OperationTypes.SpaceRace:
          break;
      }
    }

    // TODO: add points to milops counter for events/operations

    // This sets up the operation-term values for placing influence
    // and sets the active function to be the anonymous function below
    // that performs the checks for placing influence

    function operationPlaceInfluence (obj) {
      var playerCheck = actionRoundTrack.getPhasingPlayer(),
        card = players[playerCheck].getCardFromHand(obj.card),
        opsVal = card.getOpsValue(),
        counter = 0,
        arr = [];

      inputHandler.setActiveFunction(function(obj) {
        var playerName = playerCheck,
          country = gameMap.getCountry(obj.country);

        // End is the trigger to end the selection
        // without spending all available points
        if ( !obj.end ) {
          // If a country is eligible for player to place influence in
          if ( country.canPlaceInfluence(playerName) ) {
            console.log('Country ' + obj.country + ' is eligible to place influence in');

            // Get the cost to place influence in that country
            var cost = country.getCostToPlaceInfluence(playerName);
            // If the cost is too much, error
            if ( cost + counter > opsVal ) {
              console.error('ERR: Not enough operation points to place influence in this country');
            }
            // Otherwise push the country on to the array, update the ops value cost
            // And add temp influence to the country for future cost calculations
            else {
              arr.push(country);
              country.addTempInfluence(playerName);
              counter += cost;
            }
          } else {
            console.error('ERR: Country is not eligible to place influence in');
          }
        }

        // If all ops points have been spent or the user
        // wishes to end their turn without spending all influence
        if ( counter === opsVal || obj.end ) {
          // Convert the temp influence for each country to influence
          for (var i in arr) {
            arr[i].convertTempInfluence();
          }
          // End step
          thisController.updateStep();
        }
      });
    }

    // This sets up the operation-term values for realignment rolls
    // and sets the active function to be the anonymous function below
    // that performs the alignment roll operation

    function operationRealignmentRoll (obj) {
      /*
       It costs one Operations point to make a Realignment roll.
       Each player rolls a die and the high roller may remove the dif-
       ference between the rolls from their opponent’s Influence in the
       target country. Ties are considered a draw, and no markers are
       removed. Each player modifies his die roll:
       • +1 for each adjacent controlled country,
       • +1 if they have more Influence in the target country than their
       opponent,
       • +1 if your Superpower is adjacent to the target country.
       */
      var playerName = actionRoundTrack.getPhasingPlayer(),
        oppPlayerName = Utilities.getOppositePlayer(playerName),
        card = players[playerName].getCardFromHand(obj.card),
        opsVal = card.getOpsValue(),
        counter = 0;

      inputHandler.setActiveFunction(function(obj) {
        var targetCountryName = obj.country,
          targetCountry = gameMap.getCountry(targetCountryName),
          sums = [];
        console.log(playerName + ' is trying to realign ' + targetCountryName);

        // For USA and USSR
        for (var i in players) {
          sums[i] = Utilities.rollDie()[0];
          console.log('Die rolls: ' + i + ': ' + sums[i]);

          // +1 for each adjacent controlled country,
          var countryConnections = targetCountry.getAllConnections();
          for (var country in countryConnections) {
            var thisCountry = countryConnections[country];
            if ( thisCountry.getIsControlledBy(i) ) {
              console.log(i + ' controls ' + thisCountry.getName() + ' +1');
              sums[i]++;
            }
          }

          // +1 if they have more Influence in the target country than their
          // opponent
          var oppPlayer = Utilities.getOppositePlayer(i),
            inf = targetCountry.getInfluence();
          if ( inf[i] > inf[oppPlayer] ) {
            console.log(i + ' has more influence in ' + targetCountryName + ' +1');
            sums[i]++;
          }

          // +1 if your Superpower is adjacent to the target country.
          var adjacentSuperpowers = targetCountry.getSuperpowerConnections();
          console.log(gameMap.getCountry(i).getName());
          if ( adjacentSuperpowers[0]
            && adjacentSuperpowers[0] === gameMap.getCountry(i) ) {
            var msg = targetCountryName + ' is adjacent to ';
            msg += adjacentSuperpowers[0].getName() + " +1";
            console.log(msg);
            sums[i]++;
          }
        }

        console.log('total: USA', +sums[Players.USA], ' USSR', sums[Players.USSR]);
        var difference = Math.max(0, sums[playerName] - sums[oppPlayerName]);
        if ( difference ) {
          targetCountry.removeInfluence(oppPlayer, difference);
        } else console.log('No influence removed from ' + targetCountryName);
        thisController.updateStep();
      });

      inputHandler.activeFunction({
        country: Countries.WestGermany
      });
    }

    function improveDEFCONStatus () {
      if ( gameplayDebug ) console.log('\t' + defconTrack.increaseStatus());
      thisController.updateStep();
    }

    function dealCards () {
      if ( gameplayDebug ) {
        console.log('\tDealing USSR Cards');
      }
      players.USSR.drawCard(deck);
      if ( gameplayDebug ) {
        console.log('\tDealing USA Cards');
      }
      players.USA.drawCard(deck);
      thisController.updateStep();
    }

    function previousStep () {
      curStep -= 1;
      thisController.updateStep();
    }

    thisController.updateStep();
  };
});
