define([ 'modules/DebugFlags' ], function(DebugFlags) {
  return function() {
    var discardDebug = DebugFlags.discardDebug;
    var thisDiscard = this;
    var cards = [];

    // Pushes a card onto discard pile
    thisDiscard.addCard = function(card) {
      if ( discardDebug ) {
        console.log('Adding card to discard pile: ', card.getTitle());
      }
      cards.push(card);
    };

    // Empties discard pile and returns the contents
    thisDiscard.emptyPile = function() {
      if ( discardDebug ) {
        console.log('Emptying Discard Pile');
      }
      var oldDiscard = new Array(cards);
      cards = [];
      return oldDiscard;
    };

    // Debug function that prints the discard pile
    thisDiscard._debug_printDiscardPile = function() {
      if ( discardDebug ) {
        console.log('\nPrinting Discard Pile from bottom to top:');
      }
      for ( var card = 0; card < cards.length; card++ ) {
        console.log('\t' + cards[card].getTitle());
      }
    };

    if ( discardDebug ) {
      console.log('Discard Pile Created');
      console.log('--------------------');
    }
  };
});
