define([
  'modules/GameSetupSteps',
  'modules/Players',
  'modules/Countries',
  'modules/Regions',
  'modules/DebugFlags'
], function(GameSetupSteps, Players, Countries, Regions, DebugFlags) {
  return function(config) {
    var gameSetupDebug = DebugFlags.gameSetupDebug;
    var thisController = this;
    var curStep;
    var players = config.players;
    var map = config.gameMap;
    var deck = config.deck;
    var inputHandler = config.inputHandler;
    var influencePlaced = [];

    function updateStep (st) {
      curStep = st;
      if ( gameSetupDebug ) {
        console.log('--------------------');
        console.log('Game Setup: Step: ' + curStep);
        console.log('--------------------');
      }

      switch (curStep) {
        case GameSetupSteps.Shuffle:
          // DEBUG :turning off actual shuffle for now
          //deck.shuffle();
          deck._debug_specialShuffle();
          updateStep(GameSetupSteps.DealUSSR);
          break;
        case GameSetupSteps.DealUSSR:
          players.USSR.drawCard(deck);
          updateStep(GameSetupSteps.DealUSA);
          break;
        case GameSetupSteps.DealUSA:
          players.USA.drawCard(deck);
          updateStep(GameSetupSteps.USSRAutoInfluence);
          break;
        case GameSetupSteps.USSRAutoInfluence:
          placeUSSRAutoCountries();
          updateStep(GameSetupSteps.USSRManualInfluence);
          break;
        case GameSetupSteps.USSRManualInfluence:
          inputHandler.setActiveFunction(thisController.influencePlaced);
          break;
        case GameSetupSteps.USAAutoInfluence:
          placeUSAAutoCountries();
          updateStep(GameSetupSteps.USAManualInfluence);
          break;
        case GameSetupSteps.USAManualInfluence:
          influencePlaced.USSR = null;
          inputHandler.setActiveFunction(thisController.influencePlaced);
          break;
        case GameSetupSteps.BoardSetupCheck:
          influencePlaced.USA = null;
          break;
      }
    }

    function placeUSSRAutoCountries () {
      thisController.influencePlaced(Countries.Syria);
      thisController.influencePlaced(Countries.Iraq);
      thisController.influencePlaced(Countries.NorthKorea);
      thisController.influencePlaced(Countries.NorthKorea);
      thisController.influencePlaced(Countries.NorthKorea);
      thisController.influencePlaced(Countries.EastGermany);
      thisController.influencePlaced(Countries.EastGermany);
      thisController.influencePlaced(Countries.EastGermany);
      thisController.influencePlaced(Countries.Finland);
    }

    function placeUSAAutoCountries () {
      map.placeInfluence(Players.USA, Countries.Canada);
      map.placeInfluence(Players.USA, Countries.Canada);
      map.placeInfluence(Players.USA, Countries.Iran);
      map.placeInfluence(Players.USA, Countries.Israel);
      map.placeInfluence(Players.USA, Countries.Japan);
      map.placeInfluence(Players.USA, Countries.Australia);
      map.placeInfluence(Players.USA, Countries.Australia);
      map.placeInfluence(Players.USA, Countries.Australia);
      map.placeInfluence(Players.USA, Countries.Australia);
      map.placeInfluence(Players.USA, Countries.Philippines);
      map.placeInfluence(Players.USA, Countries.SouthKorea);
      map.placeInfluence(Players.USA, Countries.Panama);
      map.placeInfluence(Players.USA, Countries.SouthAfrica);
      map.placeInfluence(Players.USA, Countries.UK);
      map.placeInfluence(Players.USA, Countries.UK);
      map.placeInfluence(Players.USA, Countries.UK);
      map.placeInfluence(Players.USA, Countries.UK);
      map.placeInfluence(Players.USA, Countries.UK);
    }

    // ==================================== //
    // ========== PUBLIC METHODS ========== //
    // ==================================== //

    // Requires a player from Players object and an array of Country objects
    thisController.influencePlaced = function(inf) {
      var allowPlacement = false;
      var player;

      // If the influence is being placed in the automatic phase, it can be placed
      // outsite of Eastern Europe(USSR) or Western Europe(USA)
      if ( curStep === GameSetupSteps.USSRAutoInfluence || curStep === GameSetupSteps.USSRManualInfluence ) {
        if ( curStep === GameSetupSteps.USSRAutoInfluence ) {
          allowPlacement = true;
        }
        player = Players.USSR;
      } else if ( curStep === GameSetupSteps.USAAutoInfluence || curStep === GameSetupSteps.USAManualInfluence ) {
        if ( curStep === GameSetupSteps.USAAutoInfluence ) {
          allowPlacement = true;
        }
        player = Players.USA;
      } else {
        return;
      }

      // USSR player must place manual game setup influence in Eastern Europe
      if ( curStep === GameSetupSteps.USSRManualInfluence && map.checkCountryIsInRegion(inf, Regions.EasternEurope) ) {
        allowPlacement = true;
      }

      // USA player must place manual game setup influence in Western Europe
      if ( curStep === GameSetupSteps.USAManualInfluence && map.checkCountryIsInRegion(inf, Regions.WesternEurope) ) {
        allowPlacement = true;
      }

      // Place the influence on the map and increase the
      // setup influence counts for the correct team
      if ( allowPlacement ) {
        map.placeInfluence(player, inf);
        influencePlaced[player]++;
        if ( gameSetupDebug ) {
          console.log(player + ' placed influence in ' + inf);
        }
      }

      if ( influencePlaced.USSR === 15 ) {
        updateStep(GameSetupSteps.USAAutoInfluence);
      }
      if ( influencePlaced.USA === 25 ) {
        updateStep(GameSetupSteps.BoardSetupCheck);
      }
    };

    if ( gameSetupDebug ) {
      console.log('Game Setup Controller Created');
      console.log('--------------------');
      console.log('\nBegin game setup');
      console.log('--------------------');
    }

    // Init
    ( function() {
      influencePlaced[Players.USA] = 0;
      influencePlaced[Players.USSR] = 0;
      updateStep(GameSetupSteps.Shuffle);
    })();
  };
});
