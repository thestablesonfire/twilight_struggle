/*
 Track from 1-8 that marks which action round it is
 EARLY WAR: 6 Action rounds
 MID WAR: 7 Action rounds
 LATE WAR: 8 Action rounds
 */

define([
  'modules/Players',
  'modules/WarPeriods',
  'modules/Utilities',
  'modules/DebugFlags'
], function(Players, WarPeriods, Utilities, DebugFlags) {
  return function() {
    var actionRoundDebug = DebugFlags.actionRoundDebug;
    var thisTrack = this;
    var utilities = new Utilities();

    var MAX_ROUND = 7;
    var MIN_ROUND = 1;

    var curActionRound = 1;
    var maxActionRounds = 6;

    var phasingPlayer = Players.USSR;

    // Returns current actions round
    thisTrack.getValue = function() {
      return curActionRound;
    };

    thisTrack.getPhasingPlayer = function() {
      return phasingPlayer;
    };

    // Increases curActionRound by 1
    thisTrack.increaseActionRound = function() {
      curActionRound = Math.min(curActionRound + 1, maxActionRounds, MAX_ROUND);
      console.log('Action round has been increased to ' + curActionRound);
      phasingPlayer = utilities.getOppositePlayer(phasingPlayer);
      console.log('Phasing player set to ' + phasingPlayer);
      return curActionRound;
    };

    // Reset curActionRound to 1
    thisTrack.resetActionRounds = function() {
      curActionRound = MIN_ROUND;
      return 1;
    };

    thisTrack.getMaxActionRound = function() {
      return maxActionRounds;
    };

    // Set maxActionRounds to the correct value for the war period
    thisTrack.setMaxActionRounds = function(period) {
      if ( period === WarPeriods.Early ) {
        maxActionRounds = 6;
      } else if ( period === WarPeriods.Mid ) {
        maxActionRounds = 7;
      } else if ( period === WarPeriods.Late ) {
        maxActionRounds = 8;
      } else {
        throw "MaxActionRoundsErr";
      }
    };

    thisTrack.setPhasingPlayer = function(player) {
      console.log('Phasing player set to ' + player);
      phasingPlayer = player;
    };

    thisTrack.togglePhasingPlayer = function() {
      phasingPlayer = utilities.getOppositePlayer(phasingPlayer);
      console.log('Phasing Player is now ' + phasingPlayer);
      return phasingPlayer;
    };

    if ( actionRoundDebug ) {
      console.log('Action Round Track Created');
      console.log('--------------------');
    }
  };
});