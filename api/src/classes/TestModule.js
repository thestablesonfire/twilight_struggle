define([ 'modules/TestCases' ], function(TestCases) {
  return function() {
    var index = -1;
    var fns = [];

    this.setInput = function(input) {
      fns = new TestCases(0, input);
    };

    this.testFn = function() {
      index++;
      if ( index < fns.length ) {
        console.log('Running test scenario ' + index);
        console.log(fns[index].message);
        fns[index].test();
      }
    };
  };
});
