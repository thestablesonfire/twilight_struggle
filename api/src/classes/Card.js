define([ 'modules/DebugFlags' ], function(DebugFlags) {
  return function(num, tit, ops, type, txt) {
    var cardDebug = DebugFlags.cardDebug;
    if ( cardDebug ) {
      console.log('Card Created');
      console.log('\tNumber: ' + num);
      console.log('\tTitle: ' + tit);
      console.log('\tType: ' + type);
      console.log('\tOps Cost: ' + ops);
      console.log('\tText: ' + txt);
    }

    var thisCard = this;
    var eventType = type;
    var number = num;
    var opsValue = ops;
    var text = txt;
    var title = tit;


    // ==================================== //
    // ========== PUBLIC METHODS ========== //
    // ==================================== //

    // EFFECTS: Returns the title of the card
    thisCard.getTitle = function() {
      return title;
    };

    thisCard.getOpsValue = function() {
      return opsValue;
    };

    thisCard.getID = function() {
      var id = title.replace(/ /g, '');
      id = id.replace(/-/g, '');
      id = id.replace(/\//g, '');
      return id;
    };

    thisCard.getEventType = function() {
      return eventType;
    };
  };
});
