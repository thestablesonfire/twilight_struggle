/*
 Sliding Scale from -20(USSR) to 20(USA) where
 -20 is USSR victory
 and
 20 is USA victory
 */
define([ 'modules/DebugFlags' ], function(DebugFlags) {
  return function() {
    var vpTrackDebug = DebugFlags.vpTrackDebug;

    var thisTrack = this;

    var MAX_VP = 20;
    var MIN_VP = -20;

    var vps = 0;

    // Return current VP value of VP track
    thisTrack.getVP = function() {
      return vps;
    };

    // Increases VP toward USA victory, negative values go towards USSR victory
    thisTrack.increaseVP = function(val) {
      vps = Math.min(vps + val, MAX_VP);
      console.log('VP track score update:', val, 'final:', vps);
      return vps;
    };

    thisTrack.decreaseVP = function(val) {
      vps = Math.max(vps + val, MIN_VP);
      console.log('VP track score update:', val, 'final:', vps);
      return vps;
    };

    if ( vpTrackDebug ) {
      console.log('Victory Point Track Created');
      console.log('--------------------');
    }
  };
});
