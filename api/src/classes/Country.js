define([ 'modules/Players', 'modules/Utilities', 'modules/DebugFlags' ], function(Players, Utilities, DebugFlags) {
  return function(nam, bg, stab) {
    var countryDebug = DebugFlags.countryDebug;
    if ( countryDebug ) {
      console.log('Country Created', nam, bg, stab);
    }
    var thisCountry = this;

    var name = nam;
    var isBattleground = bg;
    var stability = stab;
    var influence = {
      USA: 0,
      USSR: 0
    };

    // Used for placing influence as an operation because you may not place
    // influence in a country you did not have influence in or adjacent to at
    // the beginning of the turn, but the cost goes down if a country changes
    // control while placing influence
    var tempInfluence = {
      USA: 0,
      USSR: 0
    };

    var regionalConnections = [];
    var interRegionalConnections = [];
    var superpowerConnections = [];

    // DEBUG
    thisCountry._debug_subConnections = function() {
      console.log('[Regional Connections]');
      for (var country in regionalConnections) {
        console.log(regionalConnections[country].getName());
      }

      console.log('[Inter-Regional Connections]');
      for (country in interRegionalConnections) {
        console.log(interRegionalConnections[country].getName());
      }

      console.log('[Superpower Connections]');
      for (country in superpowerConnections) {
        console.log(superpowerConnections[country].getName());
      }
    };

    // GETTERS
    thisCountry.getName = function() {
      return name;
    };

    thisCountry.getAllConnections = function() {
      return regionalConnections.concat(interRegionalConnections);
    };

    thisCountry.getRegionalConnections = function() {
      return regionalConnections;
    };

    thisCountry.getInterRegionalConnections = function() {
      return interRegionalConnections;
    };

    thisCountry.getSuperpowerConnections = function() {
      return superpowerConnections;
    };

    thisCountry.getInfluence = function() {
      return influence;
    };

    thisCountry.getIsBattleground = function() {
      return isBattleground;
    };

    thisCountry.getTempInfluence = function() {
      return tempInfluence;
    };

    thisCountry.addTempInfluence = function(player, num) {
      if ( num ) {
        tempInfluence[player] += num;
      }
      else tempInfluence[player]++;
    };

    // Converts the tempInfluence to influence for each player
    thisCountry.convertTempInfluence = function() {
      var USAVal = tempInfluence[Players.USA];
      if ( USAVal ) {
        console.log('Adding ' + USAVal + ' USA influence to ' + name);
      }
      influence[Players.USA] += USAVal;
      tempInfluence[Players.USA] = 0;

      var USSRVal = tempInfluence[Players.USSR];
      if ( USSRVal ) {
        console.log('Adding ' + USSRVal + ' USSR influence to ' + name);
      }
      influence[Players.USSR] += USSRVal;
      tempInfluence[Players.USSR] = 0;
    };

    // Returns whether this country is controlled by the passed player
    // countTempInf is a boolean which indicates if tempInfluence should
    // be counted
    thisCountry.getIsControlledBy = function(player, countTempInf) {
      var oppPlayer = Utilities.getOppositePlayer(player);
      var inf = influence[player];
      if ( countTempInf ) {
        inf += tempInfluence[player];
      }
      var oppInf = influence[oppPlayer];
      if ( countTempInf ) {
        oppInf += tempInfluence[oppPlayer];
      }

      return (inf >= stability) && (inf >= oppInf + stability);
    };

    thisCountry.isConnectedToSuperpower = function(superpower) {
      if ( superpowerConnections.length ) {
        if ( superpower === superpowerConnections[0].getName() ) return 1;
      }
      return 0;
    };

    // Returns a boolen indicating whether the passed player can place influece
    // in thisCountry
    thisCountry.canPlaceInfluence = function(player) {
      // If this country has player's influence
      var playerInfluence = influence[player];
      if ( playerInfluence ) return true;

      // If this country has an adjacent country with player's influence
      else {
        var allConnections = thisCountry.getAllConnections();
        for (var country in allConnections) {
          if ( allConnections.hasOwnProperty(country) && allConnections[country].getInfluence()[player] ) {
            return true;
          }
        }
      }
      return false;
    };

    // Returns the ops cost for the passed player to place an influence marker
    // in thisCountry
    thisCountry.getCostToPlaceInfluence = function(player) {
      var oppPlayer = Utilities.getOppositePlayer(player);
      // If the country is controlled by the opposing player, the cost
      // is 2
      if ( thisCountry.getIsControlledBy(oppPlayer, true) ) return 2;
      // If it is neutral or controlled by the passed player, it is 1
      else return 1;
    };

    // SETTERS
    thisCountry.setConnections = function(conns) {
      regionalConnections = conns[0];
      interRegionalConnections = conns[1];
      superpowerConnections = conns[2];
    };

    thisCountry.addInfluence = function(player, num) {
      var inf;
      !num ? inf = 1 : inf = num;
      if ( countryDebug ) {
        console.log('\t ' + inf + " " + player + ' influence added to ' + name);
      }
      influence[player] += inf;
    };

    thisCountry.removeInfluence = function(player, num) {
      var sub;
      num ? sub = num : sub = 1;
      if ( sub > influence[player] )
        sub = influence[player];
      if ( countryDebug ) {
        console.log('\t' + sub + ' ' + player + ' influence removed from ' + name);
      }
      influence[player] -= sub;
    };

    thisCountry.removeAllInfluence = function(player) {
      if ( countryDebug ) {
        console.log('\tAll ' + player + ' influence removed from ' + name);
      }
      influence[player] = 0;
    };
  };
});

/*
 LIST of countries added for convenience
 _COUNTRY_Afghanistan
 _COUNTRY_Algeria
 _COUNTRY_Angola
 _COUNTRY_Argentina
 _COUNTRY_Australia
 _COUNTRY_Austria
 _COUNTRY_Benelux
 _COUNTRY_Bolivia
 _COUNTRY_Botswana
 _COUNTRY_Brazil
 _COUNTRY_Bulgaria
 _COUNTRY_Burma
 _COUNTRY_Cameroon
 _COUNTRY_Canada
 _COUNTRY_Chile
 _COUNTRY_Colombia
 _COUNTRY_CostaRica
 _COUNTRY_Cuba
 _COUNTRY_Czechoslovakia
 _COUNTRY_Denmark
 _COUNTRY_DominicanRepublic
 _COUNTRY_EastGermany
 _COUNTRY_Ecuador
 _COUNTRY_Egypt
 _COUNTRY_ElSalvador
 _COUNTRY_Ethiopia
 _COUNTRY_Finland
 _COUNTRY_France
 _COUNTRY_Greece
 _COUNTRY_Guatemala
 _COUNTRY_GulfStates
 _COUNTRY_Haiti
 _COUNTRY_Honduras
 _COUNTRY_Hungary
 _COUNTRY_India
 _COUNTRY_Indonesia
 _COUNTRY_Iran
 _COUNTRY_Iraq
 _COUNTRY_Israel
 _COUNTRY_Italy
 _COUNTRY_IvoryCoast
 _COUNTRY_Japan
 _COUNTRY_Jordan
 _COUNTRY_Kenya
 _COUNTRY_LaosCambodia
 _COUNTRY_Lebanon
 _COUNTRY_Libya
 _COUNTRY_Malaysia
 _COUNTRY_Mexico
 _COUNTRY_Morocco
 _COUNTRY_Nicaragua
 _COUNTRY_Nigeria
 _COUNTRY_NorthKorea
 _COUNTRY_Norway
 _COUNTRY_Pakistan
 _COUNTRY_Panama
 _COUNTRY_Paraguay
 _COUNTRY_Peru
 _COUNTRY_Philipines
 _COUNTRY_Poland
 _COUNTRY_Romania
 _COUNTRY_SaharanStates
 _COUNTRY_SaudiArabia
 _COUNTRY_Somalia
 _COUNTRY_SouthAfrica
 _COUNTRY_SoutheastAfricanStates
 _COUNTRY_SouthKorea
 _COUNTRY_SpainPortugal
 _COUNTRY_Sudan
 _COUNTRY_Sweden
 _COUNTRY_Syria
 _COUNTRY_Taiwan
 _COUNTRY_Thailand
 _COUNTRY_Tunisia
 _COUNTRY_Turkey
 _COUNTRY_UK
 _COUNTRY_Uruguay
 _COUNTRY_Venezuela
 _COUNTRY_Vietnam
 _COUNTRY_WestAfricanStates
 _COUNTRY_WestGermany
 _COUNTRY_Yugoslavia
 _COUNTRY_Zaire
 _COUNTRY_Zimbabwe
 */
