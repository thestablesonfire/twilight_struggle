define([ 'modules/DebugFlags' ], function(DebugFlags) {
  // A single box in the Space Race Track
  return function(name, firstVPs, secondVPs, opsCost, rollVal, perk) {
    var spaceRaceBoxDebug = DebugFlags.spaceRaceBoxDebug;
    if ( spaceRaceBoxDebug ) {
      console.log('Space Race Box Created');
      console.log('\tName: ' + name);
      console.log('\tOps Cost: ' + opsCost);
      if ( rollVal ) {
        console.log('\tRoll Value: 1-' + rollVal);
      }
      if ( perk ) {
        console.log('\tPerk: ', perk);
      }
      if ( firstVPs ) {
        console.log('\tVPs: ' + firstVPs + '/' + secondVPs);
      }
    }

    this.name = name;
    this.opsCost = opsCost;
    this.rollVal = rollVal;
    this.perk = perk;
    this.firstVPs = firstVPs;
    this.secondVPs = secondVPs;
  };
});
