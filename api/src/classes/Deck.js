define([
  'Card',
  'TheChinaCard',
  'modules/CardTitles',
  'modules/CardTypes',
  'modules/CardTextStrings',
  'modules/DebugFlags',
  'modules/CardObjects',
  'modules/WarPeriods'
], function(Card, TheChinaCard, Cards, CardTypes, CardText, DebugFlags, CardObjects, WarPeriods) {
  return function() {
    var deckDebug = DebugFlags.deckDebug,
      thisDeck = this,
      cards = [];

    // PRIVATE METHODS

    // EFFECTS: Returns all 'Early' War Period Cards
    function getEarlyWarCards () {
      var arr = CardObjects[WarPeriods.Early];

      // TODO: Change this, I don't like it
      // Mark scoring cards
      arr[0].isScoringCard = true;
      arr[1].isScoringCard = true;
      arr[2].isScoringCard = true;

      return arr;
    }


    // ==================================== //
    // ========== PUBLIC METHODS ========== //
    // ==================================== //

    // EFFECTS: Prints the title of each card from top to bottom
    thisDeck._debug_PrintDeck = function() {
      console.log('\nPrinting Deck');
      for (var card in cards) {
        if ( cards.hasOwnProperty(card) ) {
          console.log('\t' + cards[card].getTitle());
        }
      }
    };

    // REQUIRES: At least 1 card in deck
    // EFFECTS: Takes the top card off of the deck and returns it
    thisDeck.getTopCard = function() {
      if ( deckDebug ) {
        console.log('Getting Top Card: ', cards[0].getTitle());
      }
      return cards.shift();
    };

    // Fisher-Yates algorithm from
    // http://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
    thisDeck.shuffle = function() {
      if ( deckDebug ) {
        console.log('Shuffling Deck');
      }

      var currentIndex = cards.length,
        temporaryValue,
        randomIndex;

      // While there remain elements to shuffle...
      while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex--;

        // And swap it with the current element.
        temporaryValue = cards[currentIndex];
        cards[currentIndex] = cards[randomIndex];
        cards[randomIndex] = temporaryValue;
      }
    };

    // This is a debug method to place cards in the order we want them
    thisDeck._debug_specialShuffle = function() {
      console.log('DEBUG: Placing cards in specific order in deck...');
      var tmp = cards[9];
      cards[9] = cards[4];
      cards[4] = tmp;
    };

    thisDeck.getChinaCard = function() {
      return new TheChinaCard();
    };

    ( function() {
      cards = getEarlyWarCards();
      if ( deckDebug ) {
        console.log('Deck created with ' + cards.length + ' cards');
        console.log('--------------------');
      }
    })();
  };
});
