define([ 'modules/Utilities', 'modules/DebugFlags' ], function(Utilities, DebugFlags) {
  return function(regionName, countryList, pre, dom, con) {
    var regionDebug = DebugFlags.regionDebug;
    if ( regionDebug ) {
      console.log('Region Created', regionName, countryList);
    }
    var thisRegion = this;

    var name = regionName;
    var countries = countryList;
    var subregions = [];
    var presence = pre;
    var domination = dom;
    var control = con;

    var ERR_COUNTRY_DOESNT_EXIST = 'Indicated country does not exist in countries array';

    // DEBUG
    thisRegion._debug_PrintRegion = function() {
      console.log('\n[REGION]\n' + name);

      if ( thisRegion.getHasSubregions() ) {
        console.log('[subregions]');
        for (var sr in subregions) {
          console.log(subregions[sr].getName());
        }
        console.log('\n');
      }

      for (var country in countries) {
        if ( countries.hasOwnProperty(country) ) {
          console.log(countries[country].getName());
        } else {
          console.error(ERR_COUNTRY_DOESNT_EXIST);
        }
      }
    };

    // Getters
    thisRegion.getName = function() {
      return name;
    };

    thisRegion.getHasSubregions = function() {
      return Boolean(subregions.length);
    };

    thisRegion.countryIsInRegion = function(country) {
      return Boolean(countries.indexOf(country) >= 0);
    };

    // Returns the maximum number of actions that can be taken to remove influece
    // from a player in this region with a restriction on actions per country
    thisRegion.getMaxActionsToRemoveInfluence = function(player, maxPerCountry) {
      var sum = 0;
      for (var country in countries) {
        if ( countries.hasOwnProperty(country) ) {
          var inf = countries[country].getInfluence()[player];
          sum += Math.min(inf, maxPerCountry);
        } else {
          console.error(ERR_COUNTRY_DOESNT_EXIST);
        }
      }
      return sum;
    };

    // Setters
    thisRegion.setSubregion = function(sub) {
      subregions.push(sub);
    };

    // Returns the region score from a standard
    // region scoring card for the passed player
    //      Score from presence, domination, or control
    //      +1VP per country controlled in region adjacent to enemy superpower
    //      +1VP per country controlled in region
    thisRegion.getRegionScore = function(player) {
      var score = 0;
      var oppPlayer = Utilities.getOppositePlayer(player);
      var hasPresence = playerHasPresence(player);
      var hasDomination = playerHasDomination(player);
      var hasControl = playerHasControl(player);
      if (hasControl) {
        score += control;
      }
      else if ( hasDomination ) {
        score += domination;
      }
      else if ( hasPresence ) {
        score += presence;
      }
      console.log('Presence', hasPresence, 'Domination', hasDomination, 'Control', hasControl);

      console.log('PDC:', score);

      var superpowerAdjacent = getCountriesControlledAdjacentToSuperpower(player, oppPlayer);
      console.log('superpowerAdjacent', superpowerAdjacent);
      score += superpowerAdjacent;

      var bgsControlled = getNumberControlledCountries(player, 1);
      console.log('bgsControlled: ', bgsControlled);
      score += bgsControlled;

      return score;
    };

    // Private Methods

    // Presence: has control of 1 country

    function playerHasPresence (player) {
      for (var country in countries) {
        if ( countries.hasOwnProperty(country) && countries[country].getIsControlledBy(player) ) {
          return true;
        }
      }
      return false;
    }

    // Domination:
    //  - Controls more countries
    //  - Controls more battleground countries
    //  - Must have 1 battleground and 1 non-battleground

    function playerHasDomination (player) {
      var oppPlayer = Utilities.getOppositePlayer(player);
      var playerControlled = getNumberControlledCountries(player);
      var oppControlled = getNumberControlledCountries(oppPlayer);
      var playerBGControlled = getNumberControlledCountries(player, 1);
      var oppBGControlled = getNumberControlledCountries(oppPlayer, 1);

      return (
          (playerControlled > oppControlled) &&
          (playerBGControlled > oppBGControlled) &&
          (playerControlled >= 1 && playerBGControlled >= 1)
      );
    }

    // Control
    //  - Controls more countries
    //  - Controls all battlegrounds

    function playerHasControl (player) {
      var oppPlayer = Utilities.getOppositePlayer();
      var playerControlled = getNumberControlledCountries(player);
      var playerBGControlled = getNumberControlledCountries(player, 1);
      var oppControlled = getNumberControlledCountries(oppPlayer);
      var numBG = getNumberBattlegrounds();
      return (playerControlled > oppControlled) && (playerBGControlled === numBG);
    }

    // Returns the total number of controlled countries in this region

    function getNumberControlledCountries (player, bg) {
      var sum = 0;
      for (var country in countries) {
        if ( countries.hasOwnProperty(country) && bg ) {
          if ( countries[country].getIsBattleground() && countries[country].getIsControlledBy(player) ) sum++;
        } else if ( countries[country].getIsControlledBy(player) ){
          sum++;
        }
      }
      return sum;
    }

    function getCountriesControlledAdjacentToSuperpower (player, supperpower) {
      var connected = countries.filter(function(country) {
        return country.isConnectedToSuperpower(supperpower);
      });
      var connAndOwned = connected.filter(function(country) {
        return country.getIsControlledBy(player);
      });
      return connAndOwned.length;
    }

    function getNumberBattlegrounds () {
      var sum = 0;
      for (var country in countries) {
        if ( countries.hasOwnProperty(country) && countries[country].getIsBattleground() ) sum++;
      }
      return sum;
    }
  };
});

/*
 _REGION_Africa
 _REGION_Asia
 _REGION_CentralAmerica
 _REGION_EasternEurope
 _REGION_Europe
 _REGION_MiddleEast
 _REGION_SouthAmerica
 _REGION_SoutheastAsia
 _REGION_WesternEurope
 */
