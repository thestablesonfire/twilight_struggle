define([
  'Card',
  'modules/CardTypes',
  'modules/CardTextStrings',
  'modules/Players'
], function(Card, CardTypes, CardText, Players) {
  return function() {
    var card = new Card(6, 'The China Card', 4, CardTypes.NEUTRAL, CardText.TheChinaCardText);
    card.owner = Players.USSR;
    card.played = false;
    return card;
  };
});
