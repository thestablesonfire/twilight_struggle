define([], function() {
  return function(msg, fn) {
    this.message = msg;

    this.test = fn;
  };
});