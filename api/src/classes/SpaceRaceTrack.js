define([ 'modules/SpaceRaceBoxes', 'modules/DebugFlags' ], function(SpaceRaceBoxes, DebugFlags) {
  return function() {
    var spaceRaceDebug = DebugFlags.spaceRaceDebug;
    var thisTrack = this;

    var MAX_INDEX = 8;

    var trackBoxes = [
      SpaceRaceBoxes.startBox,
      SpaceRaceBoxes.earthSatelliteBox,
      SpaceRaceBoxes.animalInSpaceBox,
      SpaceRaceBoxes.manInSpaceBox,
      SpaceRaceBoxes.manInEarthOrbit,
      SpaceRaceBoxes.lunarOrbit,
      SpaceRaceBoxes.eagleBearHasLanded,
      SpaceRaceBoxes.spaceShuttle,
      SpaceRaceBoxes.spaceStation
    ];

    var index = {
      USA: -1,
      USSR: -1
    };

    thisTrack.getPosition = function(player) {
      if ( player ) {
        return index[player];
      }
      else {
        return index;
      }
    };

    // Increase passed player's position on space race track
    thisTrack.increaseIndex = function(player) {
      index[player] = Math.min(index[player] + 1, MAX_INDEX);
      return index[player];
    };

    if ( spaceRaceDebug ) {
      console.log('Space Race Track Created');
      console.log('--------------------');
    }
  };
});
