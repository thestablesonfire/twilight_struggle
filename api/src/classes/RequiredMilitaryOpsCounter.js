define([ 'modules/DebugFlags' ], function(DebugFlags) {
  return function() {
    var milOpsDebug = DebugFlags.milOpsDebug;
    var thisMilOps = this;

    // Number of military operations
    // needed to be taken this turn
    var requiredMilOps = 5;

    // Number of military operations
    // that have been taken this turn per team
    var currentMilOps = {
      USA: 0,
      USSR: 0
    };

    // Returns the current military ops value of the passed player
    // If no passed player, returns currentMilOps
    thisMilOps.getCurrentMilOps = function(player) {
      if ( player ) {
        return currentMilOps[player];
      }
      else {
        return currentMilOps;
      }
    };

    // Returns the minimum number of military ops needed this round
    thisMilOps.getRequiredMilOps = function() {
      return requiredMilOps;
    };

    // Sets the number of required operations for the turn
    thisMilOps.setRequiredMilOps = function(req) {
      requiredMilOps = req;
    };

    // Add cur military ops to the completed military ops value for player
    thisMilOps.addCurrentMilOps = function(cur, player) {
      console.log('\tAdded ' + cur + ' milOps points to ' + player + ' milops track');
      currentMilOps[player] += cur;
    };

    // WILL NEED TO RESET ALSO

    if ( milOpsDebug ) {
      console.log('Military Ops Counter Created');
      console.log('--------------------');
    }
  };
});
