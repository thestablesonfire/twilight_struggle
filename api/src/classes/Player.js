define([ 'modules/DebugFlags' ], function(DebugFlags) {
  return function(nam) {
    var playerDebug = DebugFlags.playerDebug;
    var thisPlayer = this;

    var name = nam; // Name of the player, either USA or USSR
    var hand = []; // The held cards of the player
    var handSize = 8;

    // ==================================== //
    // ========== PUBLIC METHODS ========== //
    // ==================================== //

    // REQUIRES: a valid Deck object
    // EFFECTS: Pushes num Cards onto thisPlayer's hand,
    //          fills up hand if num is not passed
    thisPlayer.drawCard = function(deck, num) {
      var count = handSize - hand.length;
      if ( num ) {
        count = num;
      }
      for ( var i = 0; i < count; i++ ) {
        var newCard = deck.getTopCard();
        hand.push(newCard);
        if ( playerDebug ) {
          console.log("Player " + name + " drew card " + newCard.getTitle());
        }
      }

      if ( playerDebug ) {
        console.log('\nPlayer' + name + ' Hand:');
        console.log('--------------------');
        for (var card in hand) {
          console.log(hand[card].getID());
        }
      }
    };

    // Returns a specified card from the player's hand
    thisPlayer.getCardFromHand = function(card) {
      var cardIndex = thisPlayer.getCardIndexFromHandByTitle(card);
      if(~cardIndex) {
          return hand[cardIndex];
      } else {
        return null;
      }
    };

    // Randomly discards a card from the player's hand
    thisPlayer.randomlyDiscardCard = function() {
      var rndIndex = Math.floor(Math.random() * hand.length);
      // DEBUG
      rndIndex = 3;
      var card = thisPlayer.removeCardFromHand(rndIndex);
      return card;
    };

    // Discards a specific card from the player's hand
    thisPlayer.discardCard = function(cardTitle) {
      var ind = thisPlayer.getCardIndexFromHandByTitle(cardTitle);
      return thisPlayer.removeCardFromHand(ind);
    };

    // Returns the card's index in the player's hand

    thisPlayer.getCardIndexFromHandByTitle = function (title) {
      for ( var i = 0, len = hand.length; i < len; i++ ) {
        if ( title === hand[i].getTitle() ) {
          return i;
        }
      }
      return -1;
    }

    thisPlayer.removeCardFromHand = function (index) {
      console.log('Removing card from ' + name + ' player\'s hand.');
      var card = hand.splice(index, 1)[0];
      console.log('\tCARD:', card.getTitle());
      thisPlayer._debug_PrintHand();
      return card;
    }

    // DEBUG prints the current hand of the player
    thisPlayer._debug_PrintHand = function() {
      console.log('\nPrinting ' + name + ' hand\n');
      for ( var i = 0, len = hand.length; i < len; i++ ) {
        console.log('\t' + hand[i].getTitle());
      }
    };

    if ( playerDebug ) {
      console.log('Player Created: ' + nam);
      console.log('--------------------');
    }
  };
});
