define([ 'modules/DebugFlags' ], function(DebugFlags) {
  return function(test) {
    var inputHandlerDebug = DebugFlags.inputHandlerDebug;

    var activeFunction = null;
    var operationFunction = null;

    this.setActiveFunction = function(fn) {
      activeFunction = fn;
      test.testFn();
    };

    this.activeFunction = function(obj) {
      if ( activeFunction ) {
        activeFunction(obj);
      }
    };

    if ( inputHandlerDebug ) {
      console.log('Input Handler Created');
      console.log('--------------------');
    }
  };
});
