/* 
 Defcon counter is a sliding scale from 5 - 1
 5 = no restrictions
 4 = No coup or realignment attempts in Europe
 3 = No coup or realignment attempts in Europe or Asia
 2 = No coup or realignment attempts in Europe, Asia, or Middle East
 1 = NUCLEAR WAR, game immediately ends
 */
define([ 'modules/DebugFlags' ], function(DebugFlags) {
  return function() {
    var defconDebug = DebugFlags.defconDebug,
      thisTrack = this,

      // Constants
      MAX_STATUS = 5,
      MIN_STATUS = 1,

      status = MAX_STATUS;

    // Return status w/no change to status
    thisTrack.getStatus = function() {
      return status;
    };

    // Return status w/increase to the status number
    thisTrack.increaseStatus = function() {
      status = Math.min(status + 1, MAX_STATUS);
      console.log("DEFCON status increased to " + status);
      return status;
    };

    // Return status w/decrease to the status number
    thisTrack.decreaseStatus = function() {
      status = Math.max(status - 1, MIN_STATUS);
      console.log("DEFCON status decreased to " + status);
      return status;
    };

    if ( defconDebug ) {
      console.log("DEFCON Track created");
      console.log('--------------------');
    }
  };
});
