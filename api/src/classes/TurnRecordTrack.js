define([ 'modules/WarPeriods', 'modules/DebugFlags' ], function(WarPeriods, DebugFlags) {
  return function() {
    var turnRecordDebug = DebugFlags.turnRecordDebug;
    var thisTrack = this;

    var MIN_TURN = 1;
    var MAX_TURN = 10;

    var curTurn = MIN_TURN;

    thisTrack.getWarPeriod = function() {
      if ( curTurn <= 3 ) {
        return WarPeriods.Early;
      } else if ( curTurn > 3 && curTurn <= 7 ) {
        return WarPeriods.Mid;
      } else if ( curTurn > 7 ) {
        return WarPeriods.Late;
      } else {
        throw "WarPeriodErr";
      }
    };

    thisTrack.getTurn = function() {
      return curTurn;
    };

    if ( turnRecordDebug ) {
      console.log('Turn Record Track Created');
      console.log('--------------------');
    }
  };
});
