define([
  'Country',
  'Region',
  'modules/Countries',
  'modules/Players',
  'modules/Regions',
  'modules/DebugFlags'
], function(Country, Region, Countries, Players, Regions, DebugFlags) {
  return function() {
    var mapDebug = DebugFlags.mapDebug;
    var thisMap = this;

    var countries = []; // Holds all Country objects
    var regions = []; // Holds all region objects

    // ==================================== //
    // ========== PUBLIC METHODS ========== //
    // ==================================== //

    // Takes in one player and one country name
    // Adds 1 influence for the passed player
    thisMap.placeInfluence = function(player, inf) {
      if ( mapDebug ) {
        console.log(inf);
      }
      countries[inf].addInfluence(player);

      if ( mapDebug ) {
        console.log(inf, countries[inf].getInfluence());
      }
    };

    // Returns true if the passed country is in the passed region
    thisMap.checkCountryIsInRegion = function(country, region) {
      return ( regions[region].countryIsInRegion(countries[country]) );
    };

    thisMap.getRegion = function(reg) {
      return regions[reg];
    };

    thisMap.getCountry = function(country) {
      return countries[country];
    };

    // DEBUG
    thisMap._debug_PrintMap = function() {
      for (var region in regions) {
        regions[region]._debug_PrintRegion();
      }
    };

    function createCountries () {

      // TODO: Re-do this as well, it sucks

      // Superpowers
      var _COUNTRY_USA = new Country(Countries.USA, false, Infinity);
      _COUNTRY_USA.addInfluence(Players.USA, Infinity);
      var _COUNTRY_USSR = new Country(Countries.USSR, false, Infinity);
      _COUNTRY_USSR.addInfluence(Players.USSR, Infinity);

      // Create all usable countries, alphabetically
      var _COUNTRY_Afghanistan = new Country(Countries.Afghanistan, false, 2);
      var _COUNTRY_Algeria = new Country(Countries.Algeria, true, 2);
      var _COUNTRY_Angola = new Country(Countries.Angola, true, 1);
      var _COUNTRY_Argentina = new Country(Countries.Argentina, true, 2);
      var _COUNTRY_Australia = new Country(Countries.Australia, false, 4);
      var _COUNTRY_Austria = new Country(Countries.Austria, false, 4);
      var _COUNTRY_Benelux = new Country(Countries.Benelux, false, 3);
      var _COUNTRY_Bolivia = new Country(Countries.Bolivia, false, 2);
      var _COUNTRY_Botswana = new Country(Countries.Botswana, false, 2);
      var _COUNTRY_Brazil = new Country(Countries.Brazil, true, 2);
      var _COUNTRY_Bulgaria = new Country(Countries.Bulgaria, false, 3);
      var _COUNTRY_Burma = new Country(Countries.Burma, false, 2);
      var _COUNTRY_Cameroon = new Country(Countries.Cameroon, false, 1);
      var _COUNTRY_Canada = new Country(Countries.Canada, false, 4);
      var _COUNTRY_Chile = new Country(Countries.Chile, true, 3);
      var _COUNTRY_Colombia = new Country(Countries.Colombia, false, 1);
      var _COUNTRY_CostaRica = new Country(Countries.CostaRica, false, 3);
      var _COUNTRY_Cuba = new Country(Countries.Cuba, true, 3);
      var _COUNTRY_Czechoslovakia = new Country(Countries.Czechoslovakia, false, 3);
      var _COUNTRY_Denmark = new Country(Countries.Denmark, false, 3);
      var _COUNTRY_DominicanRepublic = new Country(Countries.DominicanRepublic, false, 1);
      var _COUNTRY_EastGermany = new Country(Countries.EastGermany, true, 3);
      var _COUNTRY_Ecuador = new Country(Countries.Ecudaor, false, 2);
      var _COUNTRY_Egypt = new Country(Countries.Egypt, true, 2);
      var _COUNTRY_ElSalvador = new Country(Countries.ElSalvador, false, 1);
      var _COUNTRY_Ethiopia = new Country(Countries.Ethiopia, false, 1);
      var _COUNTRY_Finland = new Country(Countries.Finland, false, 4);
      var _COUNTRY_France = new Country(Countries.France, true, 3);
      var _COUNTRY_Greece = new Country(Countries.Greece, false, 2);
      var _COUNTRY_Guatemala = new Country(Countries.Guatemala, false, 1);
      var _COUNTRY_GulfStates = new Country(Countries.GulfStates, false, 3);
      var _COUNTRY_Haiti = new Country(Countries.Haiti, false, 1);
      var _COUNTRY_Honduras = new Country(Countries.Honduras, false, 2);
      var _COUNTRY_Hungary = new Country(Countries.Hungary, false, 3);
      var _COUNTRY_India = new Country(Countries.India, true, 3);
      var _COUNTRY_Indonesia = new Country(Countries.Indonesia, false, 1);
      var _COUNTRY_Iran = new Country(Countries.Iran, true, 2);
      var _COUNTRY_Iraq = new Country(Countries.Iraq, true, 3);
      var _COUNTRY_Israel = new Country(Countries.Israel, true, 4);
      var _COUNTRY_Italy = new Country(Countries.Italy, true, 2);
      var _COUNTRY_IvoryCoast = new Country(Countries.IvoryCoast, false, 2);
      var _COUNTRY_Japan = new Country(Countries.Japan, true, 4);
      var _COUNTRY_Jordan = new Country(Countries.Jordan, false, 2);
      var _COUNTRY_Kenya = new Country(Countries.Kenya, false, 2);
      var _COUNTRY_LaosCambodia = new Country(Countries.LaosCambodia, false, 1);
      var _COUNTRY_Lebanon = new Country(Countries.Lebanon, false, 1);
      var _COUNTRY_Libya = new Country(Countries.Libya, true, 2);
      var _COUNTRY_Malaysia = new Country(Countries.Malaysia, false, 2);
      var _COUNTRY_Mexico = new Country(Countries.Mexico, true, 2);
      var _COUNTRY_Morocco = new Country(Countries.Morocco, false, 3);
      var _COUNTRY_Nicaragua = new Country(Countries.Nicaragua, false, 1);
      var _COUNTRY_Nigeria = new Country(Countries.Nigeria, true, 1);
      var _COUNTRY_NorthKorea = new Country(Countries.NorthKorea, true, 3);
      var _COUNTRY_Norway = new Country(Countries.Norway, false, 4);
      var _COUNTRY_Pakistan = new Country(Countries.Pakistan, true, 2);
      var _COUNTRY_Panama = new Country(Countries.Panama, true, 2);
      var _COUNTRY_Paraguay = new Country(Countries.Paraguay, false, 2);
      var _COUNTRY_Peru = new Country(Countries.Peru, false, 2);
      var _COUNTRY_Philippines = new Country(Countries.Philippines, false, 2);
      var _COUNTRY_Poland = new Country(Countries.Poland, true, 3);
      var _COUNTRY_Romania = new Country(Countries.Romania, false, 3);
      var _COUNTRY_SaharanStates = new Country(Countries.SaharanStates, false, 1);
      var _COUNTRY_SaudiArabia = new Country(Countries.SaudiArabia, true, 3);
      var _COUNTRY_Somalia = new Country(Countries.Somalia, false, 2);
      var _COUNTRY_SouthAfrica = new Country(Countries.SouthAfrica, true, 3);
      var _COUNTRY_SoutheastAfricanStates = new Country(Countries.SoutheasstAfricanStates, false, 1);
      var _COUNTRY_SouthKorea = new Country(Countries.SouthKorea, true, 3);
      var _COUNTRY_SpainPortugal = new Country(Countries.SpainPortugal, false, 2);
      var _COUNTRY_Sudan = new Country(Countries.Sudan, false, 1);
      var _COUNTRY_Sweden = new Country(Countries.Sweden, false, 4);
      var _COUNTRY_Syria = new Country(Countries.Syria, false, 2);
      var _COUNTRY_Taiwan = new Country(Countries.Taiwan, false, 3);
      var _COUNTRY_Thailand = new Country(Countries.Thailand, true, 2);
      var _COUNTRY_Tunisia = new Country(Countries.Tunisia, false, 2);
      var _COUNTRY_Turkey = new Country(Countries.Turkey, false, 2);
      var _COUNTRY_UK = new Country(Countries.UK, false, 5);
      var _COUNTRY_Uruguay = new Country(Countries.Uruguay, false, 2);
      var _COUNTRY_Venezuela = new Country(Countries.Venezuela, true, 2);
      var _COUNTRY_Vietnam = new Country(Countries.Vietnam, false, 1);
      var _COUNTRY_WestAfricanStates = new Country(Countries.WestAfricanStates, false, 2);
      var _COUNTRY_WestGermany = new Country(Countries.WestGermany, true, 4);
      var _COUNTRY_Yugoslavia = new Country(Countries.Yugoslavia, false, 3);
      var _COUNTRY_Zaire = new Country(Countries.Zaire, true, 1);
      var _COUNTRY_Zimbabwe = new Country(Countries.Zimbabwe, false, 1);



      // Regional, Inter-Regional, Superpower Connections for each country
      var _CONNECTIONS_Afghanistan = [ [ _COUNTRY_Pakistan ], [ _COUNTRY_Iran ], [ _COUNTRY_USSR ] ];
      var _CONNECTIONS_Algeria = [ [ _COUNTRY_Morocco, _COUNTRY_SaharanStates, _COUNTRY_Tunisia ], [ _COUNTRY_France ], [] ];
      var _CONNECTIONS_Angola = [ [ _COUNTRY_Botswana, _COUNTRY_SouthAfrica, _COUNTRY_Zaire ], [], [] ];
      var _CONNECTIONS_Argentina = [ [ _COUNTRY_Chile, _COUNTRY_Paraguay, _COUNTRY_Uruguay ], [], [] ];
      var _CONNECTIONS_Australia = [ [ _COUNTRY_Malaysia ], [], [] ];
      var _CONNECTIONS_Austria = [ [ _COUNTRY_EastGermany, _COUNTRY_Hungary, _COUNTRY_Italy, _COUNTRY_WestGermany ], [], [] ];
      var _CONNECTIONS_Benelux = [ [ _COUNTRY_WestGermany, _COUNTRY_UK ], [], [] ];
      var _CONNECTIONS_Bolivia = [ [ _COUNTRY_Paraguay, _COUNTRY_Peru ], [], [] ];
      var _CONNECTIONS_Botswana = [ [ _COUNTRY_Angola, _COUNTRY_SouthAfrica, _COUNTRY_Zimbabwe ], [], [] ];
      var _CONNECTIONS_Brazil = [ [ _COUNTRY_Uruguay, _COUNTRY_Venezuela ], [], [] ];
      var _CONNECTIONS_Bulgaria = [ [ _COUNTRY_Greece, _COUNTRY_Turkey ], [], [] ];
      var _CONNECTIONS_Burma = [ [ _COUNTRY_India, _COUNTRY_LaosCambodia ], [], [] ];
      var _CONNECTIONS_Cameroon = [ [ _COUNTRY_Nigeria, _COUNTRY_Zaire ], [], [] ];
      var _CONNECTIONS_Canada = [ [ _COUNTRY_UK ], [], [ _COUNTRY_USA ] ];
      var _CONNECTIONS_Chile = [ [ _COUNTRY_Argentina, _COUNTRY_Peru ], [], [] ];
      var _CONNECTIONS_Colombia = [ [ _COUNTRY_Ecuador, _COUNTRY_Venezuela ], [ _COUNTRY_Panama ], [] ];
      var _CONNECTIONS_CostaRica = [ [ _COUNTRY_Honduras, _COUNTRY_Nicaragua, _COUNTRY_Panama ], [], [] ];
      var _CONNECTIONS_Cuba = [ [ _COUNTRY_Haiti, _COUNTRY_Nicaragua ], [], [ _COUNTRY_USA ] ];
      var _CONNECTIONS_Czechoslovakia = [ [ _COUNTRY_EastGermany, _COUNTRY_Hungary, _COUNTRY_Poland ], [], [] ];
      var _CONNECTIONS_Denmark = [ [ _COUNTRY_Sweden, _COUNTRY_WestGermany ], [], [] ];
      var _CONNECTIONS_DominicanRepublic = [ [ _COUNTRY_Haiti ], [], [] ];
      var _CONNECTIONS_EastGermany = [ [ _COUNTRY_Austria, _COUNTRY_Czechoslovakia, _COUNTRY_Poland, _COUNTRY_WestGermany ], [], [] ];
      var _CONNECTIONS_Ecuador = [ [ _COUNTRY_Colombia, _COUNTRY_Peru ], [], [] ];
      var _CONNECTIONS_Egypt = [ [ _COUNTRY_Israel, _COUNTRY_Libya ], [ _COUNTRY_Sudan ], [] ];
      var _CONNECTIONS_ElSalvador = [ [ _COUNTRY_Guatemala, _COUNTRY_Honduras ], [], [] ];
      var _CONNECTIONS_Ethiopia = [ [ _COUNTRY_Somalia, _COUNTRY_Sudan ], [], [] ];
      var _CONNECTIONS_Finland = [ [ _COUNTRY_Sweden ], [], [ _COUNTRY_USSR ] ];
      var _CONNECTIONS_France = [ [ _COUNTRY_SpainPortugal, _COUNTRY_UK, _COUNTRY_WestGermany ], [], [ _COUNTRY_Algeria ] ];
      var _CONNECTIONS_Greece = [ [ _COUNTRY_Bulgaria, _COUNTRY_Italy, _COUNTRY_Turkey, _COUNTRY_Yugoslavia ], [], [] ];
      var _CONNECTIONS_Guatemala = [ [ _COUNTRY_ElSalvador, _COUNTRY_Honduras, _COUNTRY_Mexico ], [], [] ];
      var _CONNECTIONS_GulfStates = [ [ _COUNTRY_Iraq, _COUNTRY_SaudiArabia ], [], [] ];
      var _CONNECTIONS_Haiti = [ [ _COUNTRY_Cuba, _COUNTRY_DominicanRepublic ], [], [] ];
      var _CONNECTIONS_Honduras = [ [ _COUNTRY_CostaRica, _COUNTRY_ElSalvador, _COUNTRY_Guatemala, _COUNTRY_Nicaragua ], [], [] ];
      var _CONNECTIONS_Hungary = [ [ _COUNTRY_Austria, _COUNTRY_Czechoslovakia, _COUNTRY_Romania, _COUNTRY_Yugoslavia ], [], [] ];
      var _CONNECTIONS_India = [ [ _COUNTRY_Burma, _COUNTRY_Pakistan ], [], [] ];
      var _CONNECTIONS_Indonesia = [ [ _COUNTRY_Malaysia, _COUNTRY_Philippines ], [], [] ];
      var _CONNECTIONS_Iran = [ [ _COUNTRY_Iraq ], [ _COUNTRY_Afghanistan, _COUNTRY_Pakistan ], [] ];
      var _CONNECTIONS_Iraq = [ [ _COUNTRY_GulfStates, _COUNTRY_Iran, _COUNTRY_Jordan, _COUNTRY_SaudiArabia ], [], [] ];
      var _CONNECTIONS_Israel = [ [ _COUNTRY_Egypt, _COUNTRY_Jordan, _COUNTRY_Syria ], [], [] ];
      var _CONNECTIONS_Italy = [ [ _COUNTRY_Austria, _COUNTRY_France, _COUNTRY_Greece, _COUNTRY_SpainPortugal, _COUNTRY_Yugoslavia ], [], [] ];
      var _CONNECTIONS_IvoryCoast = [ [ _COUNTRY_Nigeria, _COUNTRY_WestAfricanStates ], [], [] ];
      var _CONNECTIONS_Japan = [ [ _COUNTRY_SouthKorea, _COUNTRY_Taiwan ], [], [ _COUNTRY_USA ] ];
      var _CONNECTIONS_Jordan = [ [ _COUNTRY_Iraq, _COUNTRY_Israel, _COUNTRY_Lebanon, _COUNTRY_SaudiArabia ], [], [] ];
      var _CONNECTIONS_Kenya = [ [ _COUNTRY_Somalia, _COUNTRY_SoutheastAfricanStates ], [], [] ];
      var _CONNECTIONS_LaosCambodia = [ [ _COUNTRY_Burma, _COUNTRY_Thailand, _COUNTRY_Vietnam ], [], [] ];
      var _CONNECTIONS_Lebanon = [ [ _COUNTRY_Israel, _COUNTRY_Syria ], [], [] ];
      var _CONNECTIONS_Libya = [ [ _COUNTRY_Egypt ], [ _COUNTRY_Tunisia ], [] ];
      var _CONNECTIONS_Malaysia = [ [ _COUNTRY_Australia, _COUNTRY_Indonesia, _COUNTRY_Thailand ], [], [] ];
      var _CONNECTIONS_Mexico = [ [ _COUNTRY_Guatemala ], [], [ _COUNTRY_USA ] ];
      var _CONNECTIONS_Morocco = [ [ _COUNTRY_Algeria, _COUNTRY_WestAfricanStates ], [ _COUNTRY_SpainPortugal ], [] ];
      var _CONNECTIONS_Nicaragua = [ [ _COUNTRY_CostaRica, _COUNTRY_Cuba, _COUNTRY_Honduras ], [], [] ];
      var _CONNECTIONS_Nigeria = [ [ _COUNTRY_Cameroon, _COUNTRY_IvoryCoast, _COUNTRY_SaharanStates ], [], [] ];
      var _CONNECTIONS_NorthKorea = [ [ _COUNTRY_SouthKorea ], [], [ _COUNTRY_USSR ] ];
      var _CONNECTIONS_Norway = [ [ _COUNTRY_Sweden, _COUNTRY_UK ], [], [] ];
      var _CONNECTIONS_Pakistan = [ [ _COUNTRY_Afghanistan, _COUNTRY_India ], [ _COUNTRY_Iran ], [] ];
      var _CONNECTIONS_Panama = [ [ _COUNTRY_CostaRica ], [], [ _COUNTRY_Colombia ] ];
      var _CONNECTIONS_Paraguay = [ [ _COUNTRY_Argentina, _COUNTRY_Bolivia, _COUNTRY_Uruguay ], [], [] ];
      var _CONNECTIONS_Peru = [ [ _COUNTRY_Bolivia, _COUNTRY_Chile, _COUNTRY_Ecuador ], [], [] ];
      var _CONNECTIONS_Philippines = [ [ _COUNTRY_Indonesia, _COUNTRY_Japan ], [], [] ];
      var _CONNECTIONS_Poland = [ [ _COUNTRY_Czechoslovakia, _COUNTRY_EastGermany, _COUNTRY_USSR ], [], [] ];
      var _CONNECTIONS_Romania = [ [ _COUNTRY_Hungary, _COUNTRY_Turkey, _COUNTRY_Yugoslavia ], [], [ _COUNTRY_USSR ] ];
      var _CONNECTIONS_SaharanStates = [ [ _COUNTRY_Algeria, _COUNTRY_Nigeria ], [], [] ];
      var _CONNECTIONS_SaudiArabia = [ [ _COUNTRY_GulfStates, _COUNTRY_Iraq, _COUNTRY_Jordan ], [], [] ];
      var _CONNECTIONS_Somalia = [ [ _COUNTRY_Ethiopia, _COUNTRY_Kenya ], [], [] ];
      var _CONNECTIONS_SouthAfrica = [ [ _COUNTRY_Angola, _COUNTRY_Botswana ], [], [] ];
      var _CONNECTIONS_SoutheastAfricanStates = [ [ _COUNTRY_Kenya, _COUNTRY_Zimbabwe ], [], [] ];
      var _CONNECTIONS_SouthKorea = [ [ _COUNTRY_Japan, _COUNTRY_NorthKorea, _COUNTRY_Taiwan ], [], [] ];
      var _CONNECTIONS_SpainPortugal = [ [ _COUNTRY_France, _COUNTRY_Italy ], [ _COUNTRY_Morocco ], [] ];
      var _CONNECTIONS_Sudan = [ [ _COUNTRY_Ethiopia ], [ _COUNTRY_Egypt ], [] ];
      var _CONNECTIONS_Sweden = [ [ _COUNTRY_Denmark, _COUNTRY_Finland, _COUNTRY_Norway ], [], [] ];
      var _CONNECTIONS_Syria = [ [ _COUNTRY_Israel, _COUNTRY_Lebanon ], [ _COUNTRY_Turkey ], [] ];
      var _CONNECTIONS_Taiwan = [ [ _COUNTRY_Japan, _COUNTRY_SouthKorea ], [], [] ];
      var _CONNECTIONS_Thailand = [ [ _COUNTRY_LaosCambodia, _COUNTRY_Malaysia, _COUNTRY_Vietnam ], [], [] ];
      var _CONNECTIONS_Tunisia = [ [ _COUNTRY_Algeria ], [ _COUNTRY_Libya ], [] ];
      var _CONNECTIONS_Turkey = [ [ _COUNTRY_Bulgaria, _COUNTRY_Greece, _COUNTRY_Romania ], [ _COUNTRY_Syria ], [] ];
      var _CONNECTIONS_UK = [ [ _COUNTRY_Benelux, _COUNTRY_Canada, _COUNTRY_France, _COUNTRY_Norway ], [], [] ];
      var _CONNECTIONS_Uruguay = [ [ _COUNTRY_Argentina, _COUNTRY_Brazil, _COUNTRY_Paraguay ], [], [] ];
      var _CONNECTIONS_Venezuela = [ [ _COUNTRY_Brazil, _COUNTRY_Colombia ], [], [] ];
      var _CONNECTIONS_Vietnam = [ [ _COUNTRY_LaosCambodia, _COUNTRY_Thailand ], [], [] ];
      var _CONNECTIONS_WestAfricanStates = [ [ _COUNTRY_IvoryCoast, _COUNTRY_Morocco ], [], [] ];
      var _CONNECTIONS_WestGermany = [ [ _COUNTRY_Austria, _COUNTRY_Benelux, _COUNTRY_Denmark, _COUNTRY_EastGermany, _COUNTRY_France ], [], [] ];
      var _CONNECTIONS_Yugoslavia = [ [ _COUNTRY_Greece, _COUNTRY_Hungary, _COUNTRY_Italy, _COUNTRY_Romania ], [], [] ];
      var _CONNECTIONS_Zaire = [ [ _COUNTRY_Angola, _COUNTRY_Cameroon, _COUNTRY_Zimbabwe ], [], [] ];
      var _CONNECTIONS_Zimbabwe = [ [ _COUNTRY_Botswana, _COUNTRY_SoutheastAfricanStates, _COUNTRY_Zaire ], [], [] ];

      // Set all of those connections
      _COUNTRY_Afghanistan.setConnections(_CONNECTIONS_Afghanistan);
      _COUNTRY_Algeria.setConnections(_CONNECTIONS_Algeria);
      _COUNTRY_Angola.setConnections(_CONNECTIONS_Angola);
      _COUNTRY_Argentina.setConnections(_CONNECTIONS_Argentina);
      _COUNTRY_Australia.setConnections(_CONNECTIONS_Australia);
      _COUNTRY_Austria.setConnections(_CONNECTIONS_Austria);
      _COUNTRY_Benelux.setConnections(_CONNECTIONS_Benelux);
      _COUNTRY_Bolivia.setConnections(_CONNECTIONS_Bolivia);
      _COUNTRY_Botswana.setConnections(_CONNECTIONS_Botswana);
      _COUNTRY_Brazil.setConnections(_CONNECTIONS_Brazil);
      _COUNTRY_Bulgaria.setConnections(_CONNECTIONS_Bulgaria);
      _COUNTRY_Burma.setConnections(_CONNECTIONS_Burma);
      _COUNTRY_Cameroon.setConnections(_CONNECTIONS_Cameroon);
      _COUNTRY_Canada.setConnections(_CONNECTIONS_Canada);
      _COUNTRY_Chile.setConnections(_CONNECTIONS_Chile);
      _COUNTRY_Colombia.setConnections(_CONNECTIONS_Colombia);
      _COUNTRY_CostaRica.setConnections(_CONNECTIONS_CostaRica);
      _COUNTRY_Cuba.setConnections(_CONNECTIONS_Cuba);
      _COUNTRY_Czechoslovakia.setConnections(_CONNECTIONS_Czechoslovakia);
      _COUNTRY_Denmark.setConnections(_CONNECTIONS_Denmark);
      _COUNTRY_DominicanRepublic.setConnections(_CONNECTIONS_DominicanRepublic);
      _COUNTRY_EastGermany.setConnections(_CONNECTIONS_EastGermany);
      _COUNTRY_Ecuador.setConnections(_CONNECTIONS_Ecuador);
      _COUNTRY_Egypt.setConnections(_CONNECTIONS_Egypt);
      _COUNTRY_ElSalvador.setConnections(_CONNECTIONS_ElSalvador);
      _COUNTRY_Ethiopia.setConnections(_CONNECTIONS_Ethiopia);
      _COUNTRY_Finland.setConnections(_CONNECTIONS_Finland);
      _COUNTRY_France.setConnections(_CONNECTIONS_France);
      _COUNTRY_Greece.setConnections(_CONNECTIONS_Greece);
      _COUNTRY_Guatemala.setConnections(_CONNECTIONS_Guatemala);
      _COUNTRY_GulfStates.setConnections(_CONNECTIONS_GulfStates);
      _COUNTRY_Haiti.setConnections(_CONNECTIONS_Haiti);
      _COUNTRY_Honduras.setConnections(_CONNECTIONS_Honduras);
      _COUNTRY_Hungary.setConnections(_CONNECTIONS_Hungary);
      _COUNTRY_India.setConnections(_CONNECTIONS_India);
      _COUNTRY_Indonesia.setConnections(_CONNECTIONS_Indonesia);
      _COUNTRY_Iran.setConnections(_CONNECTIONS_Iran);
      _COUNTRY_Iraq.setConnections(_CONNECTIONS_Iraq);
      _COUNTRY_Israel.setConnections(_CONNECTIONS_Israel);
      _COUNTRY_Italy.setConnections(_CONNECTIONS_Italy);
      _COUNTRY_IvoryCoast.setConnections(_CONNECTIONS_IvoryCoast);
      _COUNTRY_Japan.setConnections(_CONNECTIONS_Japan);
      _COUNTRY_Jordan.setConnections(_CONNECTIONS_Jordan);
      _COUNTRY_Kenya.setConnections(_CONNECTIONS_Kenya);
      _COUNTRY_LaosCambodia.setConnections(_CONNECTIONS_LaosCambodia);
      _COUNTRY_Lebanon.setConnections(_CONNECTIONS_Lebanon);
      _COUNTRY_Libya.setConnections(_CONNECTIONS_Libya);
      _COUNTRY_Malaysia.setConnections(_CONNECTIONS_Malaysia);
      _COUNTRY_Mexico.setConnections(_CONNECTIONS_Mexico);
      _COUNTRY_Morocco.setConnections(_CONNECTIONS_Morocco);
      _COUNTRY_Nicaragua.setConnections(_CONNECTIONS_Nicaragua);
      _COUNTRY_Nigeria.setConnections(_CONNECTIONS_Nigeria);
      _COUNTRY_NorthKorea.setConnections(_CONNECTIONS_NorthKorea);
      _COUNTRY_Norway.setConnections(_CONNECTIONS_Norway);
      _COUNTRY_Pakistan.setConnections(_CONNECTIONS_Pakistan);
      _COUNTRY_Panama.setConnections(_CONNECTIONS_Panama);
      _COUNTRY_Paraguay.setConnections(_CONNECTIONS_Paraguay);
      _COUNTRY_Peru.setConnections(_CONNECTIONS_Peru);
      _COUNTRY_Philippines.setConnections(_CONNECTIONS_Philippines);
      _COUNTRY_Poland.setConnections(_CONNECTIONS_Poland);
      _COUNTRY_Romania.setConnections(_CONNECTIONS_Romania);
      _COUNTRY_SaharanStates.setConnections(_CONNECTIONS_SaharanStates);
      _COUNTRY_SaudiArabia.setConnections(_CONNECTIONS_SaudiArabia);
      _COUNTRY_Somalia.setConnections(_CONNECTIONS_Somalia);
      _COUNTRY_SouthAfrica.setConnections(_CONNECTIONS_SouthAfrica);
      _COUNTRY_SoutheastAfricanStates.setConnections(_CONNECTIONS_SoutheastAfricanStates);
      _COUNTRY_SouthKorea.setConnections(_CONNECTIONS_SouthKorea);
      _COUNTRY_SpainPortugal.setConnections(_CONNECTIONS_SpainPortugal);
      _COUNTRY_Sudan.setConnections(_CONNECTIONS_Sudan);
      _COUNTRY_Sweden.setConnections(_CONNECTIONS_Sweden);
      _COUNTRY_Syria.setConnections(_CONNECTIONS_Syria);
      _COUNTRY_Taiwan.setConnections(_CONNECTIONS_Taiwan);
      _COUNTRY_Thailand.setConnections(_CONNECTIONS_Thailand);
      _COUNTRY_Tunisia.setConnections(_CONNECTIONS_Tunisia);
      _COUNTRY_Turkey.setConnections(_CONNECTIONS_Turkey);
      _COUNTRY_UK.setConnections(_CONNECTIONS_UK);
      _COUNTRY_Uruguay.setConnections(_CONNECTIONS_Uruguay);
      _COUNTRY_Venezuela.setConnections(_CONNECTIONS_Venezuela);
      _COUNTRY_Vietnam.setConnections(_CONNECTIONS_Vietnam);
      _COUNTRY_WestAfricanStates.setConnections(_CONNECTIONS_WestAfricanStates);
      _COUNTRY_WestGermany.setConnections(_CONNECTIONS_WestGermany);
      _COUNTRY_Yugoslavia.setConnections(_CONNECTIONS_Yugoslavia);
      _COUNTRY_Zaire.setConnections(_CONNECTIONS_Zaire);
      _COUNTRY_Zimbabwe.setConnections(_CONNECTIONS_Zimbabwe);

      // Set countries in countries array
      countries[Countries.USA] = _COUNTRY_USA;
      countries[Countries.USSR] = _COUNTRY_USSR;
      countries[Countries.Afghanistan] = _COUNTRY_Afghanistan;
      countries[Countries.Algeria] = _COUNTRY_Algeria;
      countries[Countries.Angola] = _COUNTRY_Angola;
      countries[Countries.Argentina] = _COUNTRY_Argentina;
      countries[Countries.Australia] = _COUNTRY_Australia;
      countries[Countries.Austria] = _COUNTRY_Austria;
      countries[Countries.Benelux] = _COUNTRY_Benelux;
      countries[Countries.Bolivia] = _COUNTRY_Bolivia;
      countries[Countries.Botswana] = _COUNTRY_Botswana;
      countries[Countries.Brazil] = _COUNTRY_Brazil;
      countries[Countries.Bulgaria] = _COUNTRY_Bulgaria;
      countries[Countries.Burma] = _COUNTRY_Burma;
      countries[Countries.Cameroon] = _COUNTRY_Cameroon;
      countries[Countries.Canada] = _COUNTRY_Canada;
      countries[Countries.Chile] = _COUNTRY_Chile;
      countries[Countries.Colombia] = _COUNTRY_Colombia;
      countries[Countries.CostaRica] = _COUNTRY_CostaRica;
      countries[Countries.Cuba] = _COUNTRY_Cuba;
      countries[Countries.Czechoslovakia] = _COUNTRY_Czechoslovakia;
      countries[Countries.Denmark] = _COUNTRY_Denmark;
      countries[Countries.DominicanRepublic] = _COUNTRY_DominicanRepublic;
      countries[Countries.EastGermany] = _COUNTRY_EastGermany;
      countries[Countries.Ecuador] = _COUNTRY_Ecuador;
      countries[Countries.Egypt] = _COUNTRY_Egypt;
      countries[Countries.ElSalvador] = _COUNTRY_ElSalvador;
      countries[Countries.Ethiopia] = _COUNTRY_Ethiopia;
      countries[Countries.Finland] = _COUNTRY_Finland;
      countries[Countries.France] = _COUNTRY_France;
      countries[Countries.Greece] = _COUNTRY_Greece;
      countries[Countries.Guatemala] = _COUNTRY_Guatemala;
      countries[Countries.GulfStates] = _COUNTRY_GulfStates;
      countries[Countries.Haiti] = _COUNTRY_Haiti;
      countries[Countries.Honduras] = _COUNTRY_Honduras;
      countries[Countries.Hungary] = _COUNTRY_Hungary;
      countries[Countries.India] = _COUNTRY_India;
      countries[Countries.Indonesia] = _COUNTRY_Indonesia;
      countries[Countries.Iran] = _COUNTRY_Iran;
      countries[Countries.Iraq] = _COUNTRY_Iraq;
      countries[Countries.Israel] = _COUNTRY_Israel;
      countries[Countries.Italy] = _COUNTRY_Italy;
      countries[Countries.IvoryCoast] = _COUNTRY_IvoryCoast;
      countries[Countries.Japan] = _COUNTRY_Japan;
      countries[Countries.Jordan] = _COUNTRY_Jordan;
      countries[Countries.Kenya] = _COUNTRY_Kenya;
      countries[Countries.LaosCambodia] = _COUNTRY_LaosCambodia;
      countries[Countries.Lebanon] = _COUNTRY_Lebanon;
      countries[Countries.Libya] = _COUNTRY_Libya;
      countries[Countries.Malaysia] = _COUNTRY_Malaysia;
      countries[Countries.Mexico] = _COUNTRY_Mexico;
      countries[Countries.Morocco] = _COUNTRY_Morocco;
      countries[Countries.Nicaragua] = _COUNTRY_Nicaragua;
      countries[Countries.Nigeria] = _COUNTRY_Nigeria;
      countries[Countries.NorthKorea] = _COUNTRY_NorthKorea;
      countries[Countries.Norway] = _COUNTRY_Norway;
      countries[Countries.Pakistan] = _COUNTRY_Pakistan;
      countries[Countries.Panama] = _COUNTRY_Panama;
      countries[Countries.Paraguay] = _COUNTRY_Paraguay;
      countries[Countries.Peru] = _COUNTRY_Peru;
      countries[Countries.Philippines] = _COUNTRY_Philippines;
      countries[Countries.Poland] = _COUNTRY_Poland;
      countries[Countries.Romania] = _COUNTRY_Romania;
      countries[Countries.SaharanStates] = _COUNTRY_SaharanStates;
      countries[Countries.SaudiArabia] = _COUNTRY_SaudiArabia;
      countries[Countries.Somalia] = _COUNTRY_Somalia;
      countries[Countries.SouthAfrica] = _COUNTRY_SouthAfrica;
      countries[Countries.SoutheastAfricanStates] = _COUNTRY_SoutheastAfricanStates;
      countries[Countries.SouthKorea] = _COUNTRY_SouthKorea;
      countries[Countries.SpainPortugal] = _COUNTRY_SpainPortugal;
      countries[Countries.Sudan] = _COUNTRY_Sudan;
      countries[Countries.Sweden] = _COUNTRY_Sweden;
      countries[Countries.Syria] = _COUNTRY_Syria;
      countries[Countries.Taiwan] = _COUNTRY_Taiwan;
      countries[Countries.Thailand] = _COUNTRY_Thailand;
      countries[Countries.Tunisia] = _COUNTRY_Tunisia;
      countries[Countries.Turkey] = _COUNTRY_Turkey;
      countries[Countries.UK] = _COUNTRY_UK;
      countries[Countries.Uruguay] = _COUNTRY_Uruguay;
      countries[Countries.Venezuela] = _COUNTRY_Venezuela;
      countries[Countries.Vietnam] = _COUNTRY_Vietnam;
      countries[Countries.WestAfricanStates] = _COUNTRY_WestAfricanStates;
      countries[Countries.WestGermany] = _COUNTRY_WestGermany;
      countries[Countries.Yugoslavia] = _COUNTRY_Yugoslavia;
      countries[Countries.Zaire] = _COUNTRY_Zaire;
      countries[Countries.Zimbabwe] = _COUNTRY_Zimbabwe;
    }
    ;

    function createRegions () {
      // List every country in each region
      var africaCountries = [
        countries[Countries.Algeria],
        countries[Countries.Angola],
        countries[Countries.Botswana],
        countries[Countries.Cameroon],
        countries[Countries.Ethiopia],
        countries[Countries.IvoryCoast],
        countries[Countries.Kenya],
        countries[Countries.Morocco],
        countries[Countries.Nigeria],
        countries[Countries.SaharanStates],
        countries[Countries.Somalia],
        countries[Countries.SouthAfrica],
        countries[Countries.SoutheastAfricanStates],
        countries[Countries.Sudan],
        countries[Countries.Tunisia],
        countries[Countries.WestAfricanStates],
        countries[Countries.Zaire],
        countries[Countries.Zimbabwe]
      ];

      var asiaCountries = [
        countries[Countries.Afghanistan],
        countries[Countries.Australia],
        countries[Countries.Burma],
        countries[Countries.India],
        countries[Countries.Indonesia],
        countries[Countries.Japan],
        countries[Countries.LaosCambodia],
        countries[Countries.Malaysia],
        countries[Countries.NorthKorea],
        countries[Countries.Pakistan],
        countries[Countries.Philippines],
        countries[Countries.SouthKorea],
        countries[Countries.Taiwan],
        countries[Countries.Thailand],
        countries[Countries.Vietnam]
      ];

      var centralAmericaCountries = [
        countries[Countries.CostaRica],
        countries[Countries.Cuba],
        countries[Countries.DominicanRepublic],
        countries[Countries.ElSalvador],
        countries[Countries.Guatemala],
        countries[Countries.Haiti],
        countries[Countries.Honduras],
        countries[Countries.Mexico],
        countries[Countries.Nicaragua],
        countries[Countries.Panama]
      ];

      var easternEuropeCountries = [
        countries[Countries.Austria],
        countries[Countries.Bulgaria],
        countries[Countries.Czechoslovakia],
        countries[Countries.EastGermany],
        countries[Countries.Finland],
        countries[Countries.Hungary],
        countries[Countries.Poland],
        countries[Countries.Romania],
        countries[Countries.Yugoslavia]
      ];

      var europeCountries = [
        countries[Countries.Austria],
        countries[Countries.Benelux],
        countries[Countries.Bulgaria],
        countries[Countries.Canada],
        countries[Countries.Czechoslovakia],
        countries[Countries.Denmark],
        countries[Countries.EastGermany],
        countries[Countries.Finland],
        countries[Countries.France],
        countries[Countries.Greece],
        countries[Countries.Hungary],
        countries[Countries.Italy],
        countries[Countries.Norway],
        countries[Countries.Poland],
        countries[Countries.Romania],
        countries[Countries.SpainPortugal],
        countries[Countries.Sweden],
        countries[Countries.Turkey],
        countries[Countries.UK],
        countries[Countries.WestGermany],
        countries[Countries.Yugoslavia]
      ];

      var middleEastCountries = [
        countries[Countries.Egypt],
        countries[Countries.GulfStates],
        countries[Countries.Iran],
        countries[Countries.Iraq],
        countries[Countries.Israel],
        countries[Countries.Jordan],
        countries[Countries.Lebanon],
        countries[Countries.Libya],
        countries[Countries.SaudiArabia],
        countries[Countries.Syria]
      ];

      var southAmericaCountries = [
        countries[Countries.Argentina],
        countries[Countries.Bolivia],
        countries[Countries.Brazil],
        countries[Countries.Chile],
        countries[Countries.Colombia],
        countries[Countries.Ecuador],
        countries[Countries.Paraguay],
        countries[Countries.Peru],
        countries[Countries.Uruguay],
        countries[Countries.Venezuela]
      ];

      var southeastAsiaCountries = [
        countries[Countries.Burma],
        countries[Countries.Indonesia],
        countries[Countries.LaosCambodia],
        countries[Countries.Malaysia],
        countries[Countries.Philippines],
        countries[Countries.Thailand],
        countries[Countries.Vietnam]
      ];

      var westernEuropeCountries = [
        countries[Countries.Austria],
        countries[Countries.Benelux],
        countries[Countries.Canada],
        countries[Countries.Denmark],
        countries[Countries.Finland],
        countries[Countries.France],
        countries[Countries.Greece],
        countries[Countries.Italy],
        countries[Countries.Norway],
        countries[Countries.SpainPortugal],
        countries[Countries.Sweden],
        countries[Countries.Turkey],
        countries[Countries.UK],
        countries[Countries.WestGermany]
      ];


      // Create regions
      var _REGION_Africa = new Region(Regions.Africa, africaCountries, 1, 4, 6);
      var _REGION_Asia = new Region(Regions.Asia, asiaCountries, 3, 7, 9);
      var _REGION_CentralAmerica = new Region(Regions.CentralAmerica, centralAmericaCountries, 1, 3, 5);
      var _REGION_EasternEurope = new Region(Regions.EasternEurope, easternEuropeCountries, 0, 0, 0);
      var _REGION_Europe = new Region(Regions.Europe, europeCountries, 3, 7, 20);
      var _REGION_MiddleEast = new Region(Regions.MiddleEast, middleEastCountries, 3, 5, 7);
      var _REGION_SouthAmerica = new Region(Regions.SouthAmerica, southAmericaCountries, 2, 5, 6);
      var _REGION_SoutheastAsia = new Region(Regions.SoutheastAsia, southeastAsiaCountries, 0, 0, 0);
      var _REGION_WesternEurope = new Region(Regions.WesternEurope, westernEuropeCountries, 0, 0, 0);

      // Asia and Europe have sub-regions
      _REGION_Asia.setSubregion(_REGION_SoutheastAsia);
      _REGION_Europe.setSubregion(_REGION_EasternEurope);
      _REGION_Europe.setSubregion(_REGION_WesternEurope);

      // Add to regions array
      regions[Regions.Africa] = _REGION_Africa;
      regions[Regions.Asia] = _REGION_Asia;
      regions[Regions.CentralAmerica] = _REGION_CentralAmerica;
      regions[Regions.EasternEurope] = _REGION_EasternEurope;
      regions[Regions.Europe] = _REGION_Europe;
      regions[Regions.MiddleEast] = _REGION_MiddleEast;
      regions[Regions.SouthAmerica] = _REGION_SouthAmerica;
      regions[Regions.SoutheastAsia] = _REGION_SoutheastAsia;
      regions[Regions.WesternEurope] = _REGION_WesternEurope;
    }
    ;

    createCountries();
    createRegions();

    if ( mapDebug ) {
      console.log('Game Map Created');
      console.log('--------------------');
    }
  };
});
