requirejs.config({
  baseUrl: 'api/src/classes',
  paths: {
    modules: '../data'
  }
});

requirejs([ 'TestModule',
  'ActionRoundTrack',
  'Deck',
  'DEFCONTrack',
  'DiscardPile',
  'RequiredMilitaryOpsCounter',
  'Player',
  'modules/Players',
  'SpaceRaceTrack',
  'TurnRecordTrack',
  'VictoryPointTrack',
  'InputHandler',
  'GameSetupController',
  'GameMap',
  'GameplayController',
  'modules/DebugFlags' ], function(
  TestModule,
  ActionRoundTrack,
  Deck,
  DEFCONTrack,
  DiscardPile,
  RequiredMilitaryOpsCounter,
  Player,
  Players,
  SpaceRaceTrack,
  TurnRecordTrack,
  VictoryPointTrack,
  InputHandler,
  GameSetupController,
  GameMap,
  GameplayController,
  DebugFlags
) {
  var TwilightStruggle = function() {
    var tsDebug = DebugFlags.tsDebug;
    var thisTS = this;

    // Concrete game components
    var actionRoundTrack,
      chinaCard,
      deck,
      defconTrack,
      discard,
      gameMap,
      milOps,
      players,
      spaceRaceTrack,
      testModule,
      turnTrack,
      victoryPointTrack,

      // Abstract game components
      inputHandler,
      gameplayController,
      setupController;

    // DEBUG
    thisTS._debug_PrintMap = function() {
      gameMap._debug_PrintMap();
    };

    thisTS._debug_PrintDeck = function() {
      deck._debug_PrintDeck();
    };

    thisTS.validateGameComponentSetup = function() {
      // Space race markers for both players must be at -1
      if ( tsDebug ) {
        console.log('\nChecking Game Setup');
        console.log('\tChecking Space Race Track');
      }

      var srt = spaceRaceTrack.getPosition();
      console.assert(srt.USSR === -1);
      console.assert(srt.USA === -1);

      if ( tsDebug ) {
        console.log('\tChecking Military Ops Counters');
      }
      console.assert(!milOps.getCurrentMilOps(Players.USSR));
      console.assert(!milOps.getCurrentMilOps(Players.USA));
      console.assert(milOps.getRequiredMilOps() === 5);

      if ( tsDebug ) {
        console.log('\tChecking Track Turn Counter');
      }
      console.assert(turnTrack.getTurn() === 1);

      if ( tsDebug ) {
        console.log('\tChecking DEFCON Track');
      }
      console.assert(defconTrack.getStatus() === 5);

      if ( tsDebug ) {
        console.log('\tChecking Victory Point Track');
      }
      console.assert(victoryPointTrack.getVP() === 0);
      var dashes = "-------------------------------";
      if ( tsDebug ) {
        console.log(dashes + '\nAll Components set up correctly\n' +
          dashes + '\n');
      }
    };

    // Input Handler functions
    thisTS.callActiveFunction = function(obj) {
      inputHandler.activeFunction(obj);
    };

    thisTS.playCardAsOperation = function(obj) {
      inputHandler.playCardAsOperation(obj);
    };

    // Places influence to the country with the name passed in
    thisTS.influencePlaced = function(countryName) {
      setupController.influencePlaced(countryName);
    };

    // Selects a headline card for the passed player
    thisTS.headlineCardSelected = function(card, player) {
      gameplayController.headlineCardSelected(card, player);
    };

    thisTS.callActiveFunction = function(dataObj) {
      inputHandler.activeFunction(dataObj);
    };

    ( function() {
      // Create component objects
      testModule = new TestModule();
      actionRoundTrack = new ActionRoundTrack();
      deck = new Deck();
      chinaCard = deck.getChinaCard();
      defconTrack = new DEFCONTrack();
      discard = new DiscardPile();
      gameMap = new GameMap();
      milOps = new RequiredMilitaryOpsCounter();
      players = {
        USA: new Player(Players.USA),
        USSR: new Player(Players.USSR)
      };
      spaceRaceTrack = new SpaceRaceTrack();
      turnTrack = new TurnRecordTrack();
      victoryPointTrack = new VictoryPointTrack();
      inputHandler = new InputHandler(testModule);
      testModule.setInput(inputHandler);
      setupController = new GameSetupController({
        players: players,
        gameMap: gameMap,
        deck: deck,
        inputHandler: inputHandler
      });

      thisTS.validateGameComponentSetup();

      gameplayController = new GameplayController({
        actionRoundTrack: actionRoundTrack,
        chinaCard: chinaCard,
        deck: deck,
        defconTrack: defconTrack,
        discard: discard,
        inputHandler: inputHandler,
        gameMap: gameMap,
        milOps: milOps,
        players: players,
        turnTrack: turnTrack,
        victoryPointTrack: victoryPointTrack
      });
    })();
  };

  var TS;

  var speedTest = false;
  if ( speedTest ) {
    var timeSum = 0;
    var repetitions = 500;
    for ( var i = 0; i < repetitions; i++ ) {
      var startTime = +new Date();
      TS = new TwilightStruggle();
      var endTime = +new Date();
      timeSum += (endTime - startTime);
    }
    console.log('Average Execute Time: ' + (timeSum / repetitions) + 'ms');
  } else {
    TS = new TwilightStruggle();
  }
});
