/*
 TURN SEQUENCE

 A. Improve DEFCON Status
 B. Deal Cards
 C. Headline Phase
 D. Action Rounds
 E. Check Military Operations Status
 F. Reveal Held Card (Tournament only)
 G. Flip ‘The China Card’
 H. Advance Turn Marker
 I. Final Scoring (after Turn 10 only)
 */
define([], function() {
  return {
    ImproveDefconStatus: 'Improve DEFCON Status',
    DealCards: 'Deal Cards',
    HeadlinePhaseA: 'Headline Phase A',
    HeadlinePhaseB: 'Headline Phase B',
    ActionRounds: 'Action Rounds',
    ActionRoundCheck: 'Action Round Check',
    CheckMilOpsStatus: 'Check Military Operations Status',
    RevealHeldCard: 'Reveal Held Card',
    FlipChinaCard: 'Flip "The China Card"',
    AdvanceTurnMarker: 'Advance Turn Marker',
    FinalScoring: 'Final Scoring'
  };
});
