define([], function() {
  return {
    PlaceInfluence: 'Place Influence',
    RealignmentRoll: 'Realignment Roll',
    CoupAttempt: 'Coup Attempt',
    SpaceRace: 'Space Race'
  };
});
