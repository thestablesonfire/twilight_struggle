define([
    'TestCase',
    'modules/Countries',
    'modules/CardTitles',
    'modules/Players'
], function(TestCase, Countries, Cards, Players) {
  return function(index, inputHandler) {
    var tests = [
      [
        new TestCase('USSR Manual Influence Placed:\n4 influence to Poland, 1 influence to East Germany, 1' +
          ' influence to Yugoslavia', function() {
            inputHandler.activeFunction(Countries.Poland);
            inputHandler.activeFunction(Countries.Poland);
            inputHandler.activeFunction(Countries.Poland);
            inputHandler.activeFunction(Countries.Poland);
            inputHandler.activeFunction(Countries.EastGermany);
            inputHandler.activeFunction(Countries.Yugoslavia);
          }),
        new TestCase('USA Manual Influence Placed:\n2 influence to West Germany, 4 influence to Italy, 1' +
          ' influence to France', function() {
            inputHandler.activeFunction(Countries.WestGermany);
            inputHandler.activeFunction(Countries.WestGermany);
            inputHandler.activeFunction(Countries.Italy);
            inputHandler.activeFunction(Countries.Italy);
            inputHandler.activeFunction(Countries.Italy);
            inputHandler.activeFunction(Countries.Italy);
            inputHandler.activeFunction(Countries.France);
          }),
          new TestCase('Playing Card for headline phase\nUSSR Player plays \'Socialist Governments\'\nAlso, attempt' +
              ' to play 2 cards for headline cards. Should ignore the second.\nUSA player plays \'Duck and Cover\',' +
              ' which is not in their hand. Should print an error. Then, USA player plays \'Five Year Plan\'',
            function() {
              inputHandler.activeFunction({card: Cards.SocialistGovernments, player: Players.USSR});
              inputHandler.activeFunction({card: Cards.SocialistGovernments, player: Players.USSR});

              inputHandler.activeFunction({card: Cards.DuckAndCover, player: Players.USA});
              inputHandler.activeFunction({card: Cards.FiveYearPlan, player: Players.USA});
              // Five year plan is played first
              // Then Socialist Governments is played, need to select 3 US influence to remove from W Germany,
              // But no more than 2 per country
              // First try 3 from the same country
              inputHandler.activeFunction({country: Countries.WestGermany});
              inputHandler.activeFunction({country: Countries.WestGermany});
              inputHandler.activeFunction({country: Countries.WestGermany});
          })
      ]
    ];

    return tests[index];
  };
});
