define([ 'modules/Players', 'modules/DebugFlags' ], function(Players, DebugFlags) {
  return function Utilities() {
    this.getOppositePlayer = function(player) {
      if ( player === Players.USA ) {
        return Players.USSR;
      } else {
        return Players.USA;
      }
    };

    // Rolls num die and returns results as an array

    this.rollDie = function(num) {
      if ( DebugFlags.utilities ) {
        console.log('Rolling Die');
      }
      var arr = [];
      if ( !num ) {
        num = 1;
      }
      for ( var i = 0; i < num; i++ ) {
        var val = Math.floor((Math.random() * 6) + 1);
        //console.log('\tRolled a ' + val);
        arr.push(val);
      }
      return arr;
    };
  };
});
