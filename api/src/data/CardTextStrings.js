define([], function() {
  return {
    AsiaScoringText: "Presence: 3; Domination: 7; Control: 9; +1 VP per controlled Battleground country in Region;" +
      " +1 VP per country controlled that is adjacent to enemy superpower; MAY NOT BE HELD!",
    EuropeScoringText: "Presence: 3; Domination: 7; Control: Automatic Victory; +1 VP per controlled Battleground" +
      " country in Region; +1 VP per country controlled that is adjacent to enemy superpower; MAY NOT BE HELD!",
    MiddleEastScoringText: "Presence: 3; Domination: 5; Control: 7; +1 VP per controlled Battleground country in" +
      " Region; MAY NOT BE HELD!",
    DuckAndCoverText: "Degrade the DEFCON level by 1. The US receives VP equal to 5 minus the current DEFCON" +
      " level.",
    FiveYearPlanText: "The USSR must randomly discard a card. If the card has a US associated Event, the Event" +
      " occurs immediately. If the card has a USSR associated Event or an Event applicable to both players, then the" +
      " card must be discarded without triggering the Event.",
    TheChinaCardText: "This card begins the game with the USSR. When played, the player receives +1 Operations to" +
      " the Operations value of this card if it uses all its Operations in Asia. It is passed to the opponent once" +
      " played. A player receives 1 VP for holding this card at the end of Turn 10.",
    SocialistGovernmentsText: "Remove a total of 3 US Influence from any countries in Western Europe (removing no" +
      " more than 2 Influence per country). This Event cannot be used after the “#83 – The Iron Lady” Event has been" +
      " played.",
    FidelText: "Remove all US Influence from Cuba. USSR adds sufficient Influence in Cuba for Control.",
    VietnamRevoltsText: "Add 2 USSR Influence to Vietnam. For the remainder of the turn, the USSR receives +1" +
      " Operations to the Operations value of a card that uses all its Operations in Southeast Asia.",
    BlockadeText: "Unless the US immediately discards a card with an Operations value of 3 or more, remove all US" +
      " Influence from West Germany.",
    KoreanWarText: "North Korea invades South Korea. Roll a die and subtract (-1) from the die roll for every US" +
      " controlled country adjacent to South Korea. On a modified die roll of 4-6, the USSR receives 2 VP and" +
      " replaces all US Influence in South Korea with USSR Influence. The USSR adds 2 to its Military Operations Track.",
    RomanianAbdicationText: "Remove all US Influence from Romania. The USSR adds sufficient Influence to Romania for" +
      " Control.",
    ArabIsraeliWarText: "Pan-Arab Coalition invades Israel. Roll a die and subtract (-1) from the die roll for" +
      " Israel, if it is US controlled, and for every US controlled country adjacent to Israel. On a modified die roll" +
      " of 4-6, the USSR receives 2 VP and replaces all US Influence in Israel with USSR Influence. The USSR adds 2 to" +
      " its Military Operations Track. This Event cannot be used after the “#65 – Camp David Accords” Event has been" +
      " played.",
    ComeconText: "Add 1 USSR Influence to each of 4 non-US controlled countries of Eastern Europe.",
    NasserText: "Add 2 USSR Influence to Egypt. The US removes half, rounded up, of its Influence from Egypt.",
    WarsawPactFormedText: "Remove all US influence from 4 countries in Eastern Europe or add 5 USSR Influence to any" +
      " countries in Eastern Europe (adding no more than 2 Influence per country). This Event allows the “#21 – NATO”" +
      " card to be played as an Event.",
    DeGaulleLeadsFranceText: "Remove 2 US Influence from France and add 1 USSR Influence to France. This Event" +
      " cancels the effect(s) of the “#21 – NATO” Event for France only.",
    CapturedNaziScientistText: "Move the Space Race Marker ahead by 1 space.",
    TrumanDoctrineText: "Remove all USSR Influence from a single uncontrolled country in Europe.",
    OlympicGamesText: "This player sponsors the Olympics. The opponent must either participate or boycott. If the" +
      " opponent participates, each player rolls a die and the sponsor adds 2 to their roll. The player with the" +
      " highest modified die roll receives 2 VP (reroll ties). If the opponent boycotts, degrade the DEFCON level by" +
      " 1 and the sponsor may conduct Operations as if they played a 4 Ops card.",
    NATOText: "The USSR cannot make Coup Attempts or Realignment rolls against any US controlled countries in" +
      " Europe. US controlled countries in Europe cannot be attacked by play of the “#36 – Brush War” Event. This" +
      " card requires prior play of either the “#16 – Warsaw Pact Formed” or “#23 – Marshall Plan” Event(s) in order" +
      " to be played as an Event.",
    IndependentRedsText: "Add US Influence to either Yugoslavia, Romania, Bulgaria, Hungary, or Czechoslovakia so" +
      " that it equals the USSR Influence in that country.",
    MarshallPlanText: "Add 1 US Influence to each of any 7 non-USSR controlled countries in Western Europe. This" +
      " Event allows the “#21 – NATO” card to be played as an Event.",
    IndoPakistaniWarText: "India invades Pakistan or vice versa (player’s choice). Roll a die and subtract (-1)" +
      " from the die roll for every enemy controlled country adjacent to the target of the invasion (India or" +
      " Pakistan). On a modified die roll of 4-6, the player receives 2 VP and replaces all the opponent’s Influence" +
      " in the target country with their Influence. The player adds 2 to its Military Operations Track.",
    ContainmentText: "All Operations cards played by the US, for the remainder of this turn, receive +1 to their" +
      " Operations value (to a maximum of 4 Operations per card).",
    CIACreatedText: "The USSR reveals their hand of cards for this turn. The US may use the Operations value of" +
      " this card to conduct Operations.",
    USJapanMutualDefensePactText: "The US adds sufficient Influence to Japan for Control. The USSR cannot make Coup" +
      " Attempts or Realignment rolls against Japan.",
    SuezCrisisText: "Remove a total of 4 US Influence from France, the United Kingdom and Israel (removing no more" +
      " than 2 Influence per country).",
    EastEuropeanUnrestText: "Early or Mid War: Remove 1 USSR Influence from 3 countries in Eastern Europe. Late" +
      " War: Remove 2 USSR Influence from 3 countries in Eastern Europe.",
    DecolonizationText: "Add 1 USSR Influence to each of any 4 countries in Africa and/or Southeast Asia.",
    RedScarePurgeText: "All Operations cards played by the opponent, for the remainder of this turn, receive -1 to" +
      " their Operations value (to a minimum value of 1 Operations point).",
    UNInterventionText: "Play this card simultaneously with a card containing an opponent’s associated Event. The" +
      " opponent’s associated Event is canceled but you may use the Operations value of the opponent’s card to" +
      " conduct Operations. This Event cannot be played during the Headline Phase.",
    DeStalinizationText: "The USSR may reallocate up to a total of 4 Influence from one or more countries to any" +
      " non-US controlled countries (adding no more than 2 Influence per country).",
    NuclearTestBanText: "The player receives VP equal to the current DEFCON level minus 2 then improves the DEFCON" +
      " level by 2.",
    FormosanResolutionText: "If this card’s Event is in effect, Taiwan will be treated as a Battleground country," +
      " for scoring purposes only, if Taiwan is US controlled when the Asia Scoring Card is played. This Event is" +
      " cancelled after the US has played the “#6 – The China Card” card.",
    DefectorsText: "The US may play this card during the Headline Phase in order to cancel the USSR Headline Event" +
      " (including a scoring card). The canceled card is placed into the discard pile. If this card is played by the" +
      " USSR during its action round, the US gains 1 VP.",
  };
});
