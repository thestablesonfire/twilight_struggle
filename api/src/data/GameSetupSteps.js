define([], function() {
  return {
    Shuffle: 'Shuffle',
    DealUSSR: 'DealUSSR',
    DealUSA: 'DealUSA',
    USSRAutoInfluence: 'USSRAutoInfluence',
    USSRManualInfluence: 'USSRManualInfluence',
    USAAutoInfluence: 'USAAutoInfluence',
    USAManualInfluence: 'USAManualInfluence',
    BoardSetupCheck: 'BoardSetupCheck'
  };
});
