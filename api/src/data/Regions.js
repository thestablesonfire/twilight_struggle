define([], function() {
  return {
    Africa: 'Africa',
    Asia: 'Asia',
    CentralAmerica: 'Central America',
    EasternEurope: 'Eastern Europe',
    Europe: 'Europe',
    MiddleEast: 'Middle East',
    SouthAmerica: 'South America',
    SoutheastAsia: 'Southeast Asia',
    WesternEurope: 'Western Europe'
  };
});
