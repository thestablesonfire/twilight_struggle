define([
  'Card',
  'modules/CardTitles',
  'modules/CardTypes',
  'modules/CardTextStrings'
], function(Card, Cards, CardTypes, CardText){
  "use strict";
  return {
    EARLY: [
      new Card(1, Cards.AsiaScoring, 0, CardTypes.NEUTRAL, CardText.AsiaScoringText),
      new Card(2, Cards.EuropeScoring, 0, CardTypes.NEUTRAL, CardText.EuropeScoringText),
      new Card(3, Cards.MiddleEastScoring, 0, CardTypes.NEUTRAL, CardText.MiddleEastScoringText),
      new Card(4, Cards.DuckAndCover, 3, CardTypes.USA, CardText.DuckAndCoverText),
      new Card(5, Cards.FiveYearPlan, 3, CardTypes.USA, CardText.FiveYearPlanText),
      new Card(7, Cards.SocialistGovernments, 3, CardTypes.USSR, CardText.SocialistGovernmentsText),
      new Card(8, Cards.Fidel, 2, CardTypes.USSR, CardText.FidelText),
      new Card(9, Cards.VietnamRevolts, 2, CardTypes.USSR, CardText.VietnamRevoltsText),
      new Card(10, Cards.Blockade, 1, CardTypes.USSR, CardText.BlockadeText),
      new Card(11, Cards.KoreanWar, 2, CardTypes.USSR, CardText.KoreanWarText),
      new Card(12, Cards.RomanianAbdication, 1, CardTypes.USSR, CardText.RomanianAbdicationText),
      new Card(13, Cards.ArabIsraeliWar, 2, CardTypes.USSR, CardText.ArabIsraeliWarText),
      new Card(14, Cards.Comecon, 3, CardTypes.USSR, CardText.ComeconText),
      new Card(15, Cards.Nasser, 1, CardTypes.USSR, CardText.NasserText),
      new Card(16, Cards.WarsawPactFormed, 3, CardTypes.USSR, CardText.WarsawPactFormedText),
      new Card(17, Cards.DeGaulleLeadsFrance, 3, CardTypes.USSR, CardText.DeGaulleLeadsFranceText),
      new Card(18, Cards.CapturedNaziScientist, 1, CardTypes.NEUTRAL, CardText.CapturedNaziScientistText),
      new Card(19, Cards.TrumanDoctrine, 1, CardTypes.USA, CardText.TrumanDoctrineText),
      new Card(20, Cards.OlympicGames, 2, CardTypes.NEUTRAL, CardText.OlympicGamesText),
      new Card(21, Cards.NATO, 4, CardTypes.USA, CardText.NATOText),
      new Card(22, Cards.IndependentReds, 2, CardTypes.USA, CardText.IndependentRedsText),
      new Card(23, Cards.MarshallPlan, 4, CardTypes.USA, CardText.MarshallPlanText),
      new Card(24, Cards.IndoPakistaniWar, 2, CardTypes.NEUTRAL, CardText.IndoPakistaniWarText),
      new Card(25, Cards.Containment, 3, CardTypes.USA, CardText.ContainmentText),
      new Card(26, Cards.CIACreated, 1, CardTypes.USA, CardText.CIACreatedText),
      new Card(27, Cards.USJapanMutualDefensePact, 4, CardTypes.USA, CardText.USJapanMutualDefensePactText),
      new Card(28, Cards.SuezCrisis, 3, CardTypes.USSR, CardText.SuezCrisisText),
      new Card(29, Cards.EastEuropeanUnrest, 3, CardTypes.USA, CardText.EastEuropeanUnrestText),
      new Card(30, Cards.Decolonization, 2, CardTypes.USSR, CardText.DecolonizationText),
      new Card(31, Cards.RedScarePurge, 4, CardTypes.NEUTRAL, CardText.RedScarePurgeText),
      new Card(32, Cards.UNIntervention, 1, CardTypes.NEUTRAL, CardText.UNInterventionText),
      new Card(33, Cards.DeStalinization, 3, CardTypes.USSR, CardText.DeStalinizationText),
      new Card(34, Cards.NuclearTestBan, 4, CardTypes.NEUTRAL, CardText.NuclearTestBanText),
      new Card(35, Cards.FormosanResolution, 2, CardTypes.USA, CardText.FormosanResolutionText),
      new Card(103, Cards.Defectors, 2, CardTypes.USA, CardText.DefectorsText)
    ]
  }
});