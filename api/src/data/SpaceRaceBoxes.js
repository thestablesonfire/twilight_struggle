define([ 'SpaceRaceBox', 'modules/SpaceRacePerks' ], function(SpaceRaceBox, SpaceRacePerks) {
  // Create Space Race Boxes
  return {
    startBox: SpaceRaceBox('Start', 0, 0, 0, 0, null),
    earthSatelliteBox: SpaceRaceBox('Earth Satellite', 2, 1, 2, 3, null),
    animalInSpaceBox: SpaceRaceBox('Animal in Space', 0, 0, 2, 4, SpaceRacePerks.AnimalInSpace),
    manInSpaceBox: SpaceRaceBox('Man in Space', 2, 0, 2, 3, null),
    manInEarthOrbit: SpaceRaceBox('Man in Earth Orbit', 0, 0, 2, 4, SpaceRacePerks.ManInEarthOrbit),
    lunarOrbit: SpaceRaceBox('Lunar Orbit', 3, 1, 3, 3, null),
    eagleBearHasLanded: SpaceRaceBox('Eagle/Bear has Landed', 0, 0, 3, 4, SpaceRacePerks.EagleBearHasLanded),
    spaceShuttle: SpaceRaceBox('Space Shuttle', 4, 2, 3, 3, null),
    spaceStation: SpaceRaceBox('Space Station', 2, 0, 4, 2, SpaceRacePerks.SpaceStation)
  };
});
