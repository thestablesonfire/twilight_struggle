define([], function() {
  return {
    tsDebug: true,
    actionRoundDebug: true,
    cardDebug: false,
    cardEffectsDebug: true,
    countryDebug: false,
    deckDebug: true,
    defconDebug: true,
    discardDebug: true,
    gameplayDebug: true,
    gameSetupDebug: true,
    inputHandlerDebug: true,
    mapDebug: true,
    milOpsDebug: true,
    playerDebug: true,
    regionDebug: false,
    spaceRaceBoxDebug: false,
    spaceRaceDebug: true,
    turnRecordDebug: true,
    utilities: true,
    vpTrackDebug: true
  };
});