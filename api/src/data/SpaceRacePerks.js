define([], function() {
  // Object that gives a static name and value to space race perks
  return {
    AnimalInSpace: "AnimalInSpace",
    ManInEarthOrbit: "ManInEarthOrbit",
    EagleBearHasLanded: "EagleBearHasLanded",
    SpaceStation: "SpaceStation"
  };
});
